import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios'
import router from './router'

const app = createApp(App)
app.config.globalProperties.$axios = axios;
axios.interceptors.request.use(config => {
  console.log(config)
  // 判断是否存在token，如果存在的话，则每个http header都加上token
  if (localStorage.wsToken && config.url !== 'https://api.seniverse.com/v3/weather/now.json?key=SXJFAM2J3ktceLO_E&location=jincheng') {//条件这么写是因为在login.vue的页面把token存入了localStorage的wxToken中
    config.headers['X-Access-Token'] = localStorage.wsToken;
  }
  return config;//赋值完后把config返回回去

}, error => {
  // 请求错误后把我们的error返回回去
  console.log(response)
  return Promise.reject(error);
})

axios.interceptors.response.use(
  response => {
    //请求成功就给它返回回去
    return response;
  },
  // 请求错误
  error => {
    if (error.response.status == 500 && error.response.data.message === 'Token失效，请重新登录') {

      localStorage.removeItem('wsToken');
      localStorage.removeItem('userInfo');
      localStorage.removeItem('teamUser');
      if(localStorage.toUrl){
        let url = {
          path: '/bind'
          , query: {redirect: localStorage.toUrl}
        }
        router.push(url)
      }else{
        router.push('/bind')
      }



    } else {
      // alert(error.response.data)
    }
    return Promise.reject(error);
  }
)
app.use(router)
app.mount('#app')
