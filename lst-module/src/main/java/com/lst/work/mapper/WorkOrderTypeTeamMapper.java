package com.lst.work.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.lst.work.entity.WorkOrderTypeTeam;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 服务项目指定处理团队
 * @Author: jeecg-boot
 * @Date:   2022-08-26
 * @Version: V1.0
 */
public interface WorkOrderTypeTeamMapper extends BaseMapper<WorkOrderTypeTeam> {

}
