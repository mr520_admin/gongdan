package com.lst.work.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.lst.work.entity.WorkOrderTypeUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 工单类型派单用户
 * @Author: jeecg-boot
 * @Date:   2022-08-12
 * @Version: V1.0
 */
public interface WorkOrderTypeUserMapper extends BaseMapper<WorkOrderTypeUser> {

}
