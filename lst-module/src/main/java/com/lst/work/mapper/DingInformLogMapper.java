package com.lst.work.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.lst.work.entity.DingInformLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 钉钉消息通知
 * @Author: jeecg-boot
 * @Date:   2022-08-08
 * @Version: V1.0
 */
public interface DingInformLogMapper extends BaseMapper<DingInformLog> {

}
