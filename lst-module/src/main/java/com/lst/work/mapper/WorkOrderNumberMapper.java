package com.lst.work.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.lst.work.entity.WorkOrderNumber;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 工单编号
 * @Author: jeecg-boot
 * @Date:   2022-08-13
 * @Version: V1.0
 */
public interface WorkOrderNumberMapper extends BaseMapper<WorkOrderNumber> {

}
