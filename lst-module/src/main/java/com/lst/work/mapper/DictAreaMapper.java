package com.lst.work.mapper;

import org.apache.ibatis.annotations.Param;
import com.lst.work.entity.DictArea;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 地区
 * @Author: jeecg-boot
 * @Date:   2022-08-30
 * @Version: V1.0
 */
public interface DictAreaMapper extends BaseMapper<DictArea> {

	/**
	 * 编辑节点状态
	 * @param id
	 * @param status
	 */
	void updateTreeNodeStatus(@Param("id") String id,@Param("status") String status);

}
