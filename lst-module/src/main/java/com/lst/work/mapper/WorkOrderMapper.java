package com.lst.work.mapper;

import java.util.HashMap;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import com.lst.work.entity.WorkOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * @Description: 工单
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
public interface WorkOrderMapper extends BaseMapper<WorkOrder> {
    public List<HashMap<String,Object>> countByType(
            @Param("alignDay") String alignDay,
            @Param("alignMon") String alignMon,
            @Param("alignYear") String alignYear
    );
    public List<HashMap<String,Object>> countByStatus(
            @Param("zaituMark") String zaituMark,
            @Param("userId") String userId

    );

    public List<HashMap<String,Object>> countAll(
            @Param("alignDay") String alignDay,
            @Param("alignMon") String alignMon,
            @Param("alignYear") String alignYear
    );

    public List<HashMap<String,Object>> countByUser(
            @Param("type") String type,
            @Param("alignDay") String alignDay,
            @Param("alignMon") String alignMon,
            @Param("alignYear") String alignYear
    );
    public List<HashMap<String,Object>> selectDupli(
            @Param("userNo") String date,
            @Param("day") int day
    );
    public List<HashMap<String,Object>> selectDupliDiy(
            @Param("duplyColumn") String duplyColumn,
            @Param("duplyVal") String duplyVal,
            @Param("day") int day
    );

    public List<HashMap<String,Object>> countDaiban(
    );

}
