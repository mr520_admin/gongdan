package com.lst.work.service;

import com.lst.work.entity.WorkOrder;
import com.baomidou.mybatisplus.extension.service.IService;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import java.util.List;

/**
 * @Description: 工单
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
public interface IWorkOrderService extends IService<WorkOrder> {
    public HSSFWorkbook exportExcel(List<WorkOrder> ents);
}
