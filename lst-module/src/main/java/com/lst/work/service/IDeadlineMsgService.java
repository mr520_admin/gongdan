package com.lst.work.service;

import com.lst.work.entity.DeadlineMsg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 超时提醒
 * @Author: jeecg-boot
 * @Date:   2022-09-03
 * @Version: V1.0
 */
public interface IDeadlineMsgService extends IService<DeadlineMsg> {

}
