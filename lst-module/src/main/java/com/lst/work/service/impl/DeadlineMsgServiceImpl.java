package com.lst.work.service.impl;

import com.lst.work.entity.DeadlineMsg;
import com.lst.work.mapper.DeadlineMsgMapper;
import com.lst.work.service.IDeadlineMsgService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 超时提醒
 * @Author: jeecg-boot
 * @Date:   2022-09-03
 * @Version: V1.0
 */
@Service
public class DeadlineMsgServiceImpl extends ServiceImpl<DeadlineMsgMapper, DeadlineMsg> implements IDeadlineMsgService {

}
