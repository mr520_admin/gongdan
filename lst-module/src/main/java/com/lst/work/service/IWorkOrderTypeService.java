package com.lst.work.service;

import com.lst.work.entity.WorkOrderType;
import com.baomidou.mybatisplus.extension.service.IService;
import com.lst.work.entity.WorkOrderTypeView;

/**
 * @Description: 工单类型
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
public interface IWorkOrderTypeService extends IService<WorkOrderType> {

    public boolean editPlus(WorkOrderTypeView workOrderTypeView);
    public boolean addPlus(WorkOrderTypeView workOrderTypeView);
}
