package com.lst.work.service.impl;

import com.lst.work.entity.Team;
import com.lst.work.mapper.TeamMapper;
import com.lst.work.service.ITeamService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 团队
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Service
public class TeamServiceImpl extends ServiceImpl<TeamMapper, Team> implements ITeamService {

}
