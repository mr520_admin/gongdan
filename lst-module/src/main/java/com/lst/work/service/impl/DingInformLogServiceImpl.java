package com.lst.work.service.impl;

import com.lst.work.entity.DingInformLog;
import com.lst.work.mapper.DingInformLogMapper;
import com.lst.work.service.IDingInformLogService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 钉钉消息通知
 * @Author: jeecg-boot
 * @Date:   2022-08-08
 * @Version: V1.0
 */
@Service
public class DingInformLogServiceImpl extends ServiceImpl<DingInformLogMapper, DingInformLog> implements IDingInformLogService {

}
