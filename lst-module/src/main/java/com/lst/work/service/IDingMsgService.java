package com.lst.work.service;

import com.lst.work.entity.DingMsg;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 钉钉消息
 * @Author: jeecg-boot
 * @Date:   2022-08-22
 * @Version: V1.0
 */
public interface IDingMsgService extends IService<DingMsg> {

}
