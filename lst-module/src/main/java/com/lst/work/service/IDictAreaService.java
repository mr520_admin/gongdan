package com.lst.work.service;

import com.lst.work.entity.DictArea;
import com.baomidou.mybatisplus.extension.service.IService;
import org.jeecg.common.exception.JeecgBootException;

/**
 * @Description: 地区
 * @Author: jeecg-boot
 * @Date:   2022-08-30
 * @Version: V1.0
 */
public interface IDictAreaService extends IService<DictArea> {

	/**根节点父ID的值*/
	public static final String ROOT_PID_VALUE = "0";
	
	/**树节点有子节点状态值*/
	public static final String HASCHILD = "1";
	
	/**树节点无子节点状态值*/
	public static final String NOCHILD = "0";

	/**新增节点*/
	void addDictArea(DictArea dictArea);
	
	/**修改节点*/
	void updateDictArea(DictArea dictArea) throws JeecgBootException;
	
	/**删除节点*/
	void deleteDictArea(String id) throws JeecgBootException;

}
