package com.lst.work.service;

import com.lst.work.entity.DingInformLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 钉钉消息通知
 * @Author: jeecg-boot
 * @Date:   2022-08-08
 * @Version: V1.0
 */
public interface IDingInformLogService extends IService<DingInformLog> {

}
