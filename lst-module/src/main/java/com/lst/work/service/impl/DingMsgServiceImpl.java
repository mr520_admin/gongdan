package com.lst.work.service.impl;

import com.lst.work.entity.DingMsg;
import com.lst.work.mapper.DingMsgMapper;
import com.lst.work.service.IDingMsgService;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

/**
 * @Description: 钉钉消息
 * @Author: jeecg-boot
 * @Date:   2022-08-22
 * @Version: V1.0
 */
@Service
public class DingMsgServiceImpl extends ServiceImpl<DingMsgMapper, DingMsg> implements IDingMsgService {

}
