package com.lst.work.service;

import com.lst.work.entity.ProblemType;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 问题类型
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
public interface IProblemTypeService extends IService<ProblemType> {

}
