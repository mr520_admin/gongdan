package com.lst.work.service;

import com.lst.work.entity.WorkOrderTypeTeam;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 服务项目指定处理团队
 * @Author: jeecg-boot
 * @Date:   2022-08-26
 * @Version: V1.0
 */
public interface IWorkOrderTypeTeamService extends IService<WorkOrderTypeTeam> {
    public void editPlus(String typeId,String teamIds);
}
