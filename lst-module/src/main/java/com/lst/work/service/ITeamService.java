package com.lst.work.service;

import com.lst.work.entity.Team;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 团队
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
public interface ITeamService extends IService<Team> {

}
