package com.lst.work.service;

import com.lst.work.entity.WorkOrderTypeDiyColumn;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 工单自定义字段
 * @Author: jeecg-boot
 * @Date:   2022-08-26
 * @Version: V1.0
 */
public interface IWorkOrderTypeDiyColumnService extends IService<WorkOrderTypeDiyColumn> {

}
