package com.lst.work.service;

import com.lst.work.entity.SysSetting;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Description: 系统设置
 * @Author: jeecg-boot
 * @Date:   2022-08-30
 * @Version: V1.0
 */
public interface ISysSettingService extends IService<SysSetting> {

}
