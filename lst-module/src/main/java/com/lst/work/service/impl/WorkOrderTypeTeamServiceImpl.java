package com.lst.work.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lst.work.entity.WorkOrderTypeTeam;
import com.lst.work.mapper.WorkOrderTypeTeamMapper;
import com.lst.work.service.IWorkOrderTypeTeamService;
import io.netty.util.internal.StringUtil;
import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description: 服务项目指定处理团队
 * @Author: jeecg-boot
 * @Date:   2022-08-26
 * @Version: V1.0
 */
@Service
public class WorkOrderTypeTeamServiceImpl extends ServiceImpl<WorkOrderTypeTeamMapper, WorkOrderTypeTeam> implements IWorkOrderTypeTeamService {

    @Override
    public void editPlus(String typeId,String teamIds){
        if(!StringUtil.isNullOrEmpty(typeId)&&!StringUtil.isNullOrEmpty(teamIds)){
            String[] teamIdArr=teamIds.split(",");
            if(null!=teamIdArr&&teamIdArr.length>0){
                QueryWrapper<WorkOrderTypeTeam> workOrderTypeUserQueryWrapper=new QueryWrapper<>();
                workOrderTypeUserQueryWrapper.eq("work_order_type",typeId);
                this.remove(workOrderTypeUserQueryWrapper);
                List<WorkOrderTypeTeam> workOrderTypeUsers=new ArrayList<>();
                for(String team : teamIdArr){
                    WorkOrderTypeTeam workOrderTypeUser=new WorkOrderTypeTeam();
                    //workOrderTypeUser.setTenantId(tenantId);
                    workOrderTypeUser.setWorkOrderType(typeId);
                    workOrderTypeUser.setTeam(team);
                    workOrderTypeUsers.add(workOrderTypeUser);
                }
                this.saveBatch(workOrderTypeUsers);
            }
        }
    }
}
