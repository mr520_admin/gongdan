package com.lst.work.controller;

import java.io.File;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.hutool.core.util.StrUtil;
//import com.alibaba.dingtalk.openapi.demo.auth.AuthHelper;
//import com.alibaba.dingtalk.openapi.demo.utils.HttpHelper;
import com.alibaba.dingtalk.openapi.demo.auth.AuthHelper;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.google.gson.JsonObject;
import com.lst.dingtalk.Sample;
import com.lst.util.AliSms;
import com.lst.util.UnderlineToCamelUtils;
import com.lst.work.entity.*;
import com.lst.work.job.SendMsgTask;
import com.lst.work.mapper.WorkOrderMapper;
import com.lst.work.service.*;
import com.lst.work.util.ExcelImpl;
import com.lst.work.util.QRCodeGenerator;
import com.lst.work.util.SendMsgThread;
import com.lst.work.util.WorkConst;
import io.netty.util.internal.StringUtil;
import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.hibernate.jdbc.Work;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.system.util.JwtUtil;
import org.jeecg.common.system.vo.LoginUser;
import org.jeecg.common.util.TokenUtils;
import org.jeecg.common.util.oConvertUtils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 工单
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Api(tags="工单")
@RestController
@RequestMapping("/work/workOrder")
@Slf4j
public class WorkOrderController extends JeecgController<WorkOrder, IWorkOrderService> {
	@Autowired
	private IWorkOrderService workOrderService;
	@Autowired
	private  IWorkOrderTypeTeamService workOrderTypeTeamService;
	 @Autowired
	 private  ISysSettingService sysSettingService;
	@Autowired
	 private IWorkOrderTypeService workOrderTypeService;
	 @Autowired
	 private ITeamUserService teamUserService;
	 @Autowired
	 private ITeamService teamService;
	 @Autowired
	private IWorkOrderNumberService workOrderNumberService;
	@Autowired
	private ISysBaseAPI sysBaseAPI;
	 @Autowired
	 private IWorkOrderTypeUserService workOrderTypeUserService;
	 @Autowired
	 private IWorkOrderRealignService workOrderRealignService;
	 @Autowired
	 private WorkOrderMapper workOrderMapper;
	 public static final int DUPLI_DAYS=60;
	 @Autowired
	 private IDingMsgService dingMsgService;
	 @Autowired
	 private IWorkOrderTypeDiyColumnService workOrderTypeDiyColumnService;

	 @Value("${jeecg.path.upload}")
	 private String upLoadPath;
	 @Value("${ws.domain}")
	 private String domain;
	
	/**
	 * 分页列表查询
	 *
	 * @param workOrder
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "工单-分页列表查询")
	@ApiOperation(value="工单-分页列表查询", notes="工单-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(WorkOrder workOrder,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		String userType=req.getParameter("userType");
		String pageType=req.getParameter("pageType");
		String countType=req.getParameter("countType");
		String countDataType=req.getParameter("countDataType");
		String dateType=req.getParameter("dateType");
		String dateValue=req.getParameter("dateValue");
		String teamId=req.getParameter("teamId");
		String user=req.getParameter("userId");
		String queryStatus=req.getParameter("queryStatus");

//		let pageType="";//发起 faqi //派单 daipaidan // 处理chuli  // 评价 pingjia   //历史 wancheng //在途 zaitu
//		let userType="";//我的 mine //管理员 admin
//		let countType=ref(null);//从统计分析进来的 工单服务项目 all 所有 其他 同workOrderType
//		let countDataType=ref(null);//从统计分析进来的 1总数 2已完成数 3评价解决数 4 60天重复数
//		let dateType=ref(null);//从统计分析进来的 日期类型
//		let dateValue=ref(null);//从统计分析进来的 日期值
//		let teamId=ref(null);//从统计分析进来的 团队Id
//		let userId=ref(null);//从统计分析进来的 用户Id


		String username = JwtUtil.getUserNameByToken(req);
		QueryWrapper<WorkOrder> queryWrapper = QueryGenerator.initQueryWrapper(workOrder, req.getParameterMap());
		String userId="";
		final List<String> types=new ArrayList<String>();
        if(!StringUtil.isNullOrEmpty(username)){
            LoginUser loginUser=sysBaseAPI.getUserByName(username);
            if(null!=loginUser) {
                userId = loginUser.getId();

            }
        }
		if("mine".equals(userType)){
					QueryWrapper<WorkOrderTypeUser> queryWrapperU=new QueryWrapper<WorkOrderTypeUser>();
					queryWrapperU.eq("align_user",userId);
					List<WorkOrderTypeUser> workOrderTypeUsers=workOrderTypeUserService.list(queryWrapperU);

					types.add("empty");
					if(null!=workOrderTypeUsers&&workOrderTypeUsers.size()>0){
						workOrderTypeUsers.stream().forEach(item->
								types.add(item.getWorkOrderType()));
					}
		}

		if("faqi".equals(pageType)){
			queryWrapper.eq("status",WorkConst.ORDER_STATUS_ADD);
			if("mine".equals(userType)){
				queryWrapper.eq("from_user",userId);
			}
		}
        if("faqi_search".equals(pageType)){
                queryWrapper.eq("from_user",userId);
        }
		if("paidan".equals(pageType)){
			queryWrapper.eq("status",WorkConst.ORDER_STATUS_ALIGN);
			if("mine".equals(userType)){

				queryWrapper.in("type",types);
			}
		}
        if("paidan_search".equals(pageType)){
                queryWrapper.eq("align_user",userId);
        }
		if("get".equals(pageType)){
			queryWrapper.eq("status", WorkConst.ORDER_STATUS_GET);
			if("mine".equals(userType)){
				queryWrapper.eq("to_user",userId);
			}
		}
		if("chuli".equals(pageType)){
			queryWrapper.eq("status", WorkConst.ORDER_STATUS_HANDLE);
			if("mine".equals(userType)){
				queryWrapper.eq("to_user",userId);
			}
		}
        if("chuli_search".equals(pageType)){
                queryWrapper.eq("to_user",userId);
        }
		if("pingjia".equals(pageType)){
			queryWrapper.eq("status",WorkConst.ORDER_STATUS_SCORE);
			if("mine".equals(userType)){
				queryWrapper.eq("from_user",userId);
			}
		}
        if("pingjia_search".equals(pageType)){
                queryWrapper.eq("score_user",userId);
        }
		if("wancheng".equals(pageType)){
			queryWrapper.eq("status",WorkConst.ORDER_STATUS_DONE);
			if("mine".equals(userType)){
				queryWrapper.apply(StrUtil.isNotBlank(userType),
						"(from_user = '" + userId + "' or to_user = '" + userId +"' or align_user = '" + userId +"' or score_user = '" + userId+"')");
			}
		}
		if("zaitu".equals(pageType)){
			queryWrapper.ne("status",WorkConst.ORDER_STATUS_DONE);
			if("mine".equals(userType)){
				queryWrapper.apply(StrUtil.isNotBlank(userType),
						"(from_user = '" + userId + "' or to_user = '" + userId +"' or align_user = '" + userId +"' or score_user = '" + userId+"')");
			}
			if(!StringUtil.isNullOrEmpty(queryStatus)){
				queryWrapper.eq("status",queryStatus);
			}else{
				log.error("状态为空，谁请求的呢");
			}
		}

		if(!StringUtil.isNullOrEmpty(countType)){
			if(!"all".equals(countType)){
				queryWrapper.eq("type",countType);
			}
		}
		if(!StringUtil.isNullOrEmpty(user)){
			queryWrapper.eq("to_user",user);
		}
		if(!StringUtil.isNullOrEmpty(user)){
			queryWrapper.eq("to_user",user);
		}
		List<String> users=new ArrayList<String>();
		users.add("empty");
		if(!StringUtil.isNullOrEmpty(teamId)){
			QueryWrapper<TeamUser> teamUserQueryWrapper=new QueryWrapper<>();
			teamUserQueryWrapper.eq("team",teamId);
			List<TeamUser> teamUsers=teamUserService.list(teamUserQueryWrapper);
			teamUsers.stream().forEach(item->{
				users.add(item.getUser());
			});
			queryWrapper.in("to_user",users);
		}

		if(!StringUtil.isNullOrEmpty(countDataType)){
			if("count_all".equals(countDataType)){
			}
			if("count_solve".equals(countDataType)){
				queryWrapper.ge("status",WorkConst.ORDER_STATUS_SCORE);
			}
			if("count_solve_verify".equals(countDataType)){
				queryWrapper.eq("status",WorkConst.ORDER_STATUS_DONE);
				queryWrapper.eq("is_solve_verify","1");
			}
			if("count_dupli60".equals(countDataType)){
				queryWrapper.gt("duplicated60",1);
			}
		}
		if(!StringUtil.isNullOrEmpty(dateType)&&!StringUtil.isNullOrEmpty(dateValue)){
			if("1".equals(dateType)){
				queryWrapper.apply(StrUtil.isNotBlank(dateValue),
						"date_format (create_time,'%Y-%m-%d') = '"+dateValue+"'");
			}
			if("2".equals(dateType)){
				queryWrapper.apply(StrUtil.isNotBlank(dateValue),
						"date_format (create_time,'%Y-%m') = '"+dateValue+"'");
			}
			if("3".equals(dateType)){
				queryWrapper.apply(StrUtil.isNotBlank(dateValue),
						"date_format (create_time,'%Y') = '"+dateValue+"'");
			}
		}

		Page<WorkOrder> page = new Page<WorkOrder>(pageNo, pageSize);
		IPage<WorkOrder> pageList = workOrderService.page(page, queryWrapper);

		List<JSONObject> rel=new ArrayList<JSONObject>();
		IPage<JSONObject> pageListRel=new Page();
		pageListRel.setTotal(pageList.getTotal());
		pageListRel.setSize(pageList.getSize());
		pageListRel.setPages(pageList.getPages());
		pageListRel.setCurrent(pageList.getCurrent());
		if(null!=pageList&&null!=pageList.getRecords()&&pageList.getRecords().size()>0){

			rel=sysBaseAPI.parseDictTextList(pageList.getRecords());

			//扩展字段展示
			QueryWrapper<WorkOrderTypeDiyColumn> workOrderTypeDiyColumnQueryWrapper=new QueryWrapper<WorkOrderTypeDiyColumn>();
			workOrderTypeDiyColumnQueryWrapper.eq("is_show","1");
			workOrderTypeDiyColumnQueryWrapper.eq("list_show","1");
			workOrderTypeDiyColumnQueryWrapper.eq("component_type",WorkConst.COMPONENT_TYPE_XIALAKUANG);
			workOrderTypeDiyColumnQueryWrapper.eq("scope",WorkConst.DIY_SCOPE_BAOZHANG);
			workOrderTypeDiyColumnQueryWrapper.orderByAsc("order_num");
			List<WorkOrderTypeDiyColumn> workOrderTypeDiyColumns=workOrderTypeDiyColumnService.list(workOrderTypeDiyColumnQueryWrapper);
			for(JSONObject wo:rel){
				if(null!=workOrderTypeDiyColumns&&workOrderTypeDiyColumns.size()>0){
					for(WorkOrderTypeDiyColumn workOrderTypeDiyColumn:workOrderTypeDiyColumns){
						if(workOrderTypeDiyColumn.getWorkOrderType().equals(null!=wo.get("type")?(String)wo.get("type"):"")){
							String columnCode=workOrderTypeDiyColumn.getColumnCode();
							String dictJson=workOrderTypeDiyColumn.getDictJson();
							String val_dictText="";
							String val="";

							if(!StringUtil.isNullOrEmpty(columnCode)&&null!=wo.get(columnCode)){
								val = (String) wo.get(columnCode);


								if(!StringUtil.isNullOrEmpty(val)){
									JSONArray jsonArray=JSONArray.parseArray(dictJson);
									if(null!=jsonArray&&jsonArray.size()>0){
										String[] strs=val.split(",");
										if(null!=strs&&strs.length>0){
											for(String str:strs){
												for(int i=0;i<jsonArray.size();i++){
													JSONObject jsonObject=(JSONObject) jsonArray.get(i);
													if(null!=jsonObject){

														if(null!=jsonObject.get("value")&&null!=jsonObject.get("label")){
															if(str.equals((String) jsonObject.get("value"))){
																String text=(String)jsonObject.get("label");
																val_dictText+=text+",";
																break;
															}
														}
													}
												}
											}
										}

									}
									if(val_dictText.endsWith(",")){
										val_dictText=val_dictText.substring(0,val_dictText.length()-1);
									}
									wo.put(columnCode + "_dictText", val_dictText);
								}

							}
						}

					}
				}


			}
			pageListRel.setRecords(rel);
		}
		return Result.ok(pageListRel);
	}

	 /**
	  * 获取对象指定属性的值
	  * @param o  对象
	  * @param fieldName   要获取值的属性
	  * 返回值：对象指定属性的值
	  */
	 public static Object getFieldValueByName(Object o, String fieldName) {
		 try {
			 String firstLetter = fieldName.substring(0, 1).toUpperCase();
			 String getter = "get" + firstLetter + fieldName.substring(1);
			 Method method = o.getClass().getMethod(getter, new Class[] {});
			 Object value = method.invoke(o, new Object[] {});
			 return value;
		 } catch (Exception e) {
			 System.out.println(e.getMessage());
			 return null;
		 }
	 }




	 /**
	  * 获取配置信息
	  *
	  * @param req
	  * @return
	  */
	 //@AutoLog(value = "获取配置信息")
	 @ApiOperation(value="获取配置信息", notes="获取配置信息")
	 @GetMapping(value = "/getConfig")
	 public Result<?> getConfig(
									HttpServletRequest req) {
		 String domain="";
		 String agentId="";
		 String appKey="";
		 String appSecret="";
		 String corpId="";
		 List<SysSetting> sysSettings=sysSettingService.list();
		 if(null!=sysSettings&&sysSettings.size()>0) {
			 for (SysSetting sysSetting : sysSettings) {
				 if ("1".equals(sysSetting.getIsValid())) {
					 if ("domain".equals(sysSetting.getCode())) {
						 domain = sysSetting.getValue();
					 }
					 if ("agentId".equals(sysSetting.getCode())) {
						 agentId = sysSetting.getValue();
					 }
					 if ("appKey".equals(sysSetting.getCode())) {
						 appKey = sysSetting.getValue();
					 }
					 if ("appSecret".equals(sysSetting.getCode())) {
						 appSecret = sysSetting.getValue();
					 }
					 if ("corpId".equals(sysSetting.getCode())) {
						 corpId = sysSetting.getValue();
					 }
				 }
			 }
		 }
		 String url=req.getParameter("front_url");
		 Map<String,Object> rel= AuthHelper.getConfig(url,corpId,agentId,appKey,appSecret);
		 return Result.ok(rel);
	 }


	 /**
	 *   添加
	 *
	 * @param workOrder
	 * @return
	 */
	//@AutoLog(value = "工单-添加")
	@ApiOperation(value="工单-添加", notes="工单-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody WorkOrder workOrder) {
		 String domain="";
		 String agentId="";
		 String appKey="";
		 String appSecret="";

		List<SysSetting> sysSettings=sysSettingService.list();
		if(null!=sysSettings&&sysSettings.size()>0){
			for(SysSetting sysSetting:sysSettings){
				if("1".equals(sysSetting.getIsValid())){
					if("domain".equals(sysSetting.getCode())){
						domain=sysSetting.getValue();
					}
					if("agentId".equals(sysSetting.getCode())){
						agentId=sysSetting.getValue();
					}
					if("appKey".equals(sysSetting.getCode())){
						appKey=sysSetting.getValue();
					}
					if("appSecret".equals(sysSetting.getCode())){
						appSecret=sysSetting.getValue();
					}
				}
			}
		}else{
			log.error("数据异常");
		}
		String number="";
		try{

			number=workOrderNumberService.getMax(workOrder.getType());
			workOrder.setNumber(number);
			workOrder.setStatus(WorkConst.ORDER_STATUS_ALIGN);
			workOrder.setToAlignTime(new Date());
			String duplyColumnUnderling="";
				int day=0-this.DUPLI_DAYS;
				String duplyColumn="";
				String duplyVal="";
				QueryWrapper<WorkOrderTypeDiyColumn> workOrderTypeDiyColumnQueryWrapper=new QueryWrapper<WorkOrderTypeDiyColumn>();
				workOrderTypeDiyColumnQueryWrapper.eq("work_order_type",workOrder.getType());
				workOrderTypeDiyColumnQueryWrapper.eq("is_show","1");
				workOrderTypeDiyColumnQueryWrapper.eq("duplicated_mark","1");
				workOrderTypeDiyColumnQueryWrapper.orderByAsc("order_num");
				List<WorkOrderTypeDiyColumn> workOrderTypeDiyColumns=workOrderTypeDiyColumnService.list(workOrderTypeDiyColumnQueryWrapper);
				String defaultToUser="";
				String defaultDuplyToUser="";
				WorkOrderType workOrderType=workOrderTypeService.getById(workOrder.getType());
				if(null!=workOrderType){
					defaultToUser=workOrderType.getDefaultToUser();
					defaultDuplyToUser=workOrderType.getDefaultDuplyToUser();

				}


				if(null!=workOrderTypeDiyColumns&&workOrderTypeDiyColumns.size()>0){
					duplyColumn= workOrderTypeDiyColumns.get(0).getColumnCode();
					Object obj=getFieldValueByName(workOrder, duplyColumn);
					if(null!=obj){
						duplyVal=(String)obj;
					}
					duplyColumnUnderling= UnderlineToCamelUtils.camelToUnderline(duplyColumn);

				}
				String duplyMark="0";
				if(!StringUtil.isNullOrEmpty(duplyColumnUnderling)&&!StringUtil.isNullOrEmpty(duplyVal)) {
					List<HashMap<String, Object>> rel = workOrderMapper.selectDupliDiy(duplyColumnUnderling, duplyVal, day);

					if (null != rel && rel.size() > 0) {
						Long count = (Long) rel.get(0).get("count");
						int dupliVal=count.intValue();
						dupliVal++;
						workOrder.setDuplicated60(dupliVal);
						if(dupliVal>1){
							duplyMark="1";
						}
					}
				}
				String msgType=WorkConst.MSG_TYPE_PAIDAN;
				if(!StringUtil.isNullOrEmpty(defaultDuplyToUser)&&"1".equals(duplyMark)){
						workOrder.setAlignTime(new Date());
						workOrder.setFirstAlignTime(new Date());
						workOrder.setAlignUser("robot");
						workOrder.setToUser(defaultDuplyToUser);
						workOrder.setStatus(WorkConst.ORDER_STATUS_GET);
						workOrderService.save(workOrder);

					WorkOrderRealign workOrderRealign=new WorkOrderRealign();
						workOrderRealign.setWorkOrder(workOrder.getId());
						workOrderRealign.setType(WorkConst.PAIDAN_STATUS_PAIDAN);
						workOrderRealign.setFromUser("robot");
						workOrderRealign.setToUser(defaultToUser);
						workOrderRealign.setAlignTime(new Date());
						workOrderRealign.setRealignReason("系统自动派单【重复订单】");
						String json=JSONObject.toJSONString(workOrder);
						workOrderRealign.setJson(json);
						workOrderRealignService.save(workOrderRealign);
						msgType=WorkConst.MSG_TYPE_GET;

				}else if(!StringUtil.isNullOrEmpty(defaultToUser)){
					workOrder.setAlignTime(new Date());
					workOrder.setAlignUser("robot");
					workOrder.setToUser(defaultToUser);
					workOrder.setStatus(WorkConst.ORDER_STATUS_GET);
					workOrderService.save(workOrder);

					WorkOrderRealign workOrderRealign=new WorkOrderRealign();
					workOrderRealign.setWorkOrder(workOrder.getId());
					workOrderRealign.setType(WorkConst.PAIDAN_STATUS_PAIDAN);
					workOrderRealign.setFromUser("robot");
					workOrderRealign.setToUser(defaultToUser);
					workOrderRealign.setAlignTime(new Date());
					workOrderRealign.setRealignReason("系统自动派单");
					String json=JSONObject.toJSONString(workOrder);
					workOrderRealign.setJson(json);
					workOrderRealignService.save(workOrderRealign);
					msgType=WorkConst.MSG_TYPE_GET;
				}else{
					workOrderService.save(workOrder);
				}

			try {
				new SendMsgThread(workOrder.getId(),msgType,domain,agentId,appKey,appSecret,workOrderService,sysBaseAPI,workOrderTypeUserService,dingMsgService,workOrderTypeDiyColumnService,0
				,WorkConst.DING_MSG_TYPE_COMMON,1).start();
			}catch (Exception e){
				log.error(e.getMessage(),e);
			}
			if(WorkConst.baozhangType_kehu.equals(workOrder.getBaozhangType())){
				HashMap<String,String> param=new HashMap<>();
				param.put("code",workOrder.getId()+"_1");
				AliSms.sendSms(WorkConst.sms_template_shouli,JSON.toJSONString(param),workOrder.getContactPhone());
			}
		}catch (Exception e){
			log.error(e.getMessage());
			return Result.error("系统错误，联系管理员");
		}
		return Result.ok("添加成功！工单编号:"+number);
	}

	/**
	 *  编辑
	 *
	 * @param workOrder
	 * @return
	 */
	//@AutoLog(value = "工单-编辑")
	@ApiOperation(value="工单-编辑", notes="工单-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody WorkOrder workOrder) {
		workOrderService.updateById(workOrder);
		return Result.ok("编辑成功!");
	}

	public String parseDict(WorkOrderRealign workOrderRealign){
//		QueryWrapper<WorkOrderTypeDiyColumn> workOrderTypeDiyColumnQueryWrapper=new QueryWrapper<WorkOrderTypeDiyColumn>();
//		workOrderTypeDiyColumnQueryWrapper.eq("is_show","1");
//		workOrderTypeDiyColumnQueryWrapper.orderByAsc("order_num");
//		List<WorkOrderTypeDiyColumn> workOrderTypeDiyColumns=workOrderTypeDiyColumnService.list(workOrderTypeDiyColumnQueryWrapper);
		List<HashMap<String,Object>> baozhangScope=new ArrayList<HashMap<String,Object>>();
		List<HashMap<String,Object>> chuliScope=new ArrayList<HashMap<String,Object>>();
		List<HashMap<String,Object>> pingjiaScope=new ArrayList<HashMap<String,Object>>();
		String json="";
			if(null!=workOrderRealign){
				json=workOrderRealign.getJson();
			}
			WorkOrder workOrder =!StringUtil.isNullOrEmpty(json)?JSON.parseObject(json, WorkOrder.class):null;
			JSONObject jsonObj=sysBaseAPI.parseDictTextOjb(workOrder);
			if(WorkConst.PAIDAN_STATUS_CHULI.equals(workOrderRealign.getType())){
				QueryWrapper<WorkOrderTypeDiyColumn> workOrderTypeDiyColumnQueryWrapper=new QueryWrapper<WorkOrderTypeDiyColumn>();
				workOrderTypeDiyColumnQueryWrapper.eq("is_show","1");
				workOrderTypeDiyColumnQueryWrapper.orderByAsc("order_num");
				List<WorkOrderTypeDiyColumn> workOrderTypeDiyColumns=workOrderTypeDiyColumnService.list(workOrderTypeDiyColumnQueryWrapper);

				if(null!=workOrderTypeDiyColumns&&workOrderTypeDiyColumns.size()>0){
					for(WorkOrderTypeDiyColumn workOrderTypeDiyColumn:workOrderTypeDiyColumns){
						String scope=workOrderTypeDiyColumn.getScope();
						String workOrderType=workOrderTypeDiyColumn.getWorkOrderType();
						String columnCode=workOrderTypeDiyColumn.getColumnCode();
						String componentType=workOrderTypeDiyColumn.getComponentType();
						String columnName=workOrderTypeDiyColumn.getColumnName();
						String autoInputFormColumn=workOrderTypeDiyColumn.getAutoInputFromColumn();
						String duplicatedMark=workOrderTypeDiyColumn.getDuplicatedMark();
						String multiple=workOrderTypeDiyColumn.getMultiple();
						String dictJson=workOrderTypeDiyColumn.getDictJson();
						String val_dictText="";
						HashMap<String,Object> column=new HashMap<>();
						if(workOrder.getType().equals(workOrderType)){
							Object val=jsonObj.get(columnCode);
							if(null!=val){
								if(WorkConst.COMPONENT_TYPE_WENBEN.equals(componentType)||
										WorkConst.COMPONENT_TYPE_DUOHANGWENBEN.equals(componentType)||
										WorkConst.COMPONENT_TYPE_RIQI.equals(componentType)||
										WorkConst.COMPONENT_TYPE_SHIJIAN.equals(componentType)
								){
									val_dictText=val.toString();
								}
								if(WorkConst.COMPONENT_TYPE_TUPIAN.equals(componentType)||
										WorkConst.COMPONENT_TYPE_WENJIAN.equals(componentType)){
									String[] arr=val.toString().split(",");
									for(String str:arr){
										if(!StringUtil.isNullOrEmpty(str)){
											val_dictText+="/jeecgboot/lst-boot/sys/common/static/"+str;
										}

									}
									if(val_dictText.endsWith(",")){
										val_dictText=val_dictText.substring(0,val_dictText.length()-1);
									}

								}
								if(WorkConst.COMPONENT_TYPE_XIALAKUANG.equals(componentType)){
									String[] strs=null;
									if(isArray(val)){
										strs=(String[])val;
									}else{
										strs=val.toString().split(",");
									}
									JSONArray jsonArray=JSONArray.parseArray(dictJson);
									if(null!=jsonArray&&jsonArray.size()>0){
										if(null!=strs&&strs.length>0){
											for(String str:strs){
												for(int i=0;i<jsonArray.size();i++){
													JSONObject jsonObject=(JSONObject) jsonArray.get(i);
													if(null!=jsonObject){

														if(null!=jsonObject.get("value")&&null!=jsonObject.get("label")){
															if(str.equals((String) jsonObject.get("value"))){
																String text=(String)jsonObject.get("label");
																val_dictText+=text+",";
																break;
															}
														}
													}
												}
											}
										}

									}
									if(val_dictText.endsWith(",")){
										val_dictText=val_dictText.substring(0,val_dictText.length()-1);
									}
								}
								column.put("value",val);
								column.put("text",val_dictText);

							}
							column.put("columnCode",columnCode);
							column.put("columnName",columnName);

							WorkOrderTypeDiyColumnView workOrderTypeDiyColumnView = new WorkOrderTypeDiyColumnView();
							BeanUtils.copyProperties(workOrderTypeDiyColumn, workOrderTypeDiyColumnView);
							JSONArray jsonArray = JSONArray.parseArray(workOrderTypeDiyColumn.getDictJson());
							workOrderTypeDiyColumnView.setDictArr(jsonArray);
							column.put("diy",workOrderTypeDiyColumnView);
							if(WorkConst.DIY_SCOPE_CHULI.equals(scope)){
								chuliScope.add(column);
							}
						}


					}
				}
			}
		jsonObj.put("chuliScope",chuliScope);
		return jsonObj.toJSONString();
	}
	 /**
	  *  编辑
	  *
	  * @return
	  */
	 //@AutoLog(value = "工单处理")
	 @ApiOperation(value="工单处理", notes="工单处理")
	 @PostMapping(value = "/handle")
	 public Result<?> handle(@RequestBody WorkOrder workOrder, HttpServletRequest req) {
		 String domain="";
		 String agentId="";
		 String appKey="";
		 String appSecret="";
		String msgMark="";
		 QueryWrapper<WorkOrderRealign> workOrderRealignQueryWrapper=new QueryWrapper<>();
		 workOrderRealignQueryWrapper.eq("work_order",workOrder.getId());
		 workOrderRealignQueryWrapper.orderByDesc("create_time");
		WorkOrderRealign lastStep=null;
		List<WorkOrderRealign> workOrderRealigns=workOrderRealignService.list(workOrderRealignQueryWrapper);
		if(null!=workOrderRealigns&&workOrderRealigns.size()>0){
			lastStep=workOrderRealigns.get(0);
		}
		Date beginTime=null;
		if(null!=lastStep){
			beginTime=lastStep.getCreateTime();
		}else{
			beginTime=workOrder.getCreateTime();
		}
		 List<SysSetting> sysSettings=sysSettingService.list();
		 if(null!=sysSettings&&sysSettings.size()>0){
			 for(SysSetting sysSetting:sysSettings){
				 if("1".equals(sysSetting.getIsValid())){
					 if("domain".equals(sysSetting.getCode())){
						 domain=sysSetting.getValue();
					 }
					 if("agentId".equals(sysSetting.getCode())){
						 agentId=sysSetting.getValue();
					 }
					 if("appKey".equals(sysSetting.getCode())){
						 appKey=sysSetting.getValue();
					 }
					 if("appSecret".equals(sysSetting.getCode())){
						 appSecret=sysSetting.getValue();
					 }
				 }
			 }
		 }else{
			 log.error("数据异常");
		 }
	 	String status=workOrder.getStatus();
		 String userId=req.getParameter("userId");
		 String toUserId=req.getParameter("toUserId");
	 	String alignType=req.getParameter("alignType");
		 String sendMsgType="";

		 if(WorkConst.PAIDAN_STATUS_CHEHUI.equals(alignType)){
			 workOrder=workOrderService.getById(workOrder.getId());
			 if(null!=workOrder){
				 //状态转到派单
				 workOrder.setStatus(WorkConst.ORDER_STATUS_ALIGN);
				 workOrder.setToAlignTime(new Date());

				 WorkOrderRealign workOrderRealign=new WorkOrderRealign();
				 workOrderRealign.setWorkOrder(workOrder.getId());
				 workOrderRealign.setType(alignType);
				 workOrderRealign.setFromUser(userId);
				 workOrderRealign.setAlignTime(new Date());
//			 workOrderRealign.setRealignReason("");
				 String json=JSONObject.toJSONString(workOrder);
				 workOrderRealign.setJson(json);
				 workOrderRealign.setBeginTime(beginTime);
				 workOrderRealignService.save(workOrderRealign);
				 sendMsgType=WorkConst.MSG_TYPE_PAIDAN;
			 }else{
			 	return Result.error("工单不存在");
			 }


		 }else{
			 if(WorkConst.ORDER_STATUS_ALIGN.equals(status)){
				 //派单
				 // 主表
				 WorkOrderRealign workOrderRealign=new WorkOrderRealign();
				 if("1".equals(workOrder.getQuickHandleMark())){
					 workOrder.setStatus(WorkConst.ORDER_STATUS_SCORE);
					 workOrder.setSolveTime(new Date());
					 sendMsgType=WorkConst.MSG_TYPE_QUEREN;
					 msgMark=WorkConst.sms_template_wancheng;
					 workOrderRealign.setRealignReason(workOrder.getQuickHandleRemark());
					 workOrderRealign.setType(WorkConst.PAIDAN_STATUS_PAIDANSUBAN);
				 }else{
					 workOrder.setStatus(WorkConst.ORDER_STATUS_GET);
					 sendMsgType=WorkConst.MSG_TYPE_GET;
					 workOrderRealign.setType(WorkConst.PAIDAN_STATUS_PAIDAN);
					 workOrderRealign.setToUser(toUserId);
					 workOrder.setAlignTime(new Date());
					 workOrder.setAlignUser(userId);
					 workOrder.setToUser(toUserId);
					 workOrder.setFirstAlignTime(new Date());
				 }
				 //派单记录表
				 workOrderRealign.setWorkOrder(workOrder.getId());
				 workOrderRealign.setFromUser(userId);

				 workOrderRealign.setAlignTime(new Date());
				 String json=JSONObject.toJSONString(workOrder);
				 workOrderRealign.setJson(json);
				 workOrderRealign.setBeginTime(beginTime);
				 workOrderRealignService.save(workOrderRealign);


			 }
			 if(WorkConst.ORDER_STATUS_GET.equals(status)){


				 workOrder.setGetTime(new Date());
				 workOrder.setStatus(WorkConst.ORDER_STATUS_HANDLE);
				 //派单记录表
				 WorkOrderRealign workOrderRealign=new WorkOrderRealign();
				 workOrderRealign.setWorkOrder(workOrder.getId());
				 workOrderRealign.setType(WorkConst.PAIDAN_STATUS_GET);
				 workOrderRealign.setToUser(userId);
				 workOrderRealign.setAlignTime(new Date());
				 String json=JSONObject.toJSONString(workOrder);
				 workOrderRealign.setJson(json);
				 workOrderRealign.setBeginTime(beginTime);
				 workOrderRealignService.save(workOrderRealign);
				 sendMsgType=WorkConst.MSG_TYPE_CHULI;
				 msgMark=WorkConst.sms_template_jiedan;

			 }
			 if(WorkConst.ORDER_STATUS_HANDLE.equals(status)){
				 //处理

				 if(alignType.equals(WorkConst.PAIDAN_STATUS_CHULI)){
					 //解决
					 //处理主表
					 workOrder.setSolveTime(new Date());
					 workOrder.setStatus(WorkConst.ORDER_STATUS_SCORE);


					 WorkOrderRealign workOrderRealign=new WorkOrderRealign();
					 workOrderRealign.setWorkOrder(workOrder.getId());
					 workOrderRealign.setType(alignType);
					 workOrderRealign.setFromUser(userId);
					 workOrderRealign.setToUser(toUserId);
					 workOrderRealign.setAlignTime(new Date());
					 workOrderRealign.setRealignReason(workOrder.getSolveRemark());
					 String json=JSONObject.toJSONString(workOrder);
					 workOrderRealign.setJson(json);
					 workOrderRealign.setBeginTime(beginTime);
					 String jsonParse=parseDict(workOrderRealign);
					 workOrderRealign.setJson(jsonParse);
					 workOrderRealignService.save(workOrderRealign);

					 sendMsgType=WorkConst.MSG_TYPE_QUEREN;
					msgMark=WorkConst.sms_template_wancheng;
				 }else if(alignType.equals(WorkConst.PAIDAN_STATUS_ZHUANDAN)){
					 //转单
					 workOrder.setAlignTime(new Date());
					 workOrder.setToUser(toUserId);
					 workOrder.setAlignUser(userId);
					 workOrder.setStatus(WorkConst.ORDER_STATUS_GET);
					 sendMsgType=WorkConst.MSG_TYPE_GET;
					 //转单流水表
					 WorkOrderRealign workOrderRealign=new WorkOrderRealign();
					 workOrderRealign.setWorkOrder(workOrder.getId());
					 workOrderRealign.setType(alignType);
					 workOrderRealign.setFromUser(userId);
					 workOrderRealign.setToUser(toUserId);
					 workOrderRealign.setAlignTime(new Date());
					 workOrderRealign.setRealignReason(workOrder.getSolveRemark());
					 sendMsgType=WorkConst.MSG_TYPE_CHULI;
					 String json=JSONObject.toJSONString(workOrder);
					 workOrderRealign.setJson(json);
					 workOrderRealign.setBeginTime(beginTime);
					 workOrderRealignService.save(workOrderRealign);

				 }else if(alignType.equals(WorkConst.PAIDAN_STATUS_TUIDAN)){
					 //退单

					 WorkOrderRealign workOrderRealign=new WorkOrderRealign();
					 workOrderRealign.setWorkOrder(workOrder.getId());
					 workOrderRealign.setType(alignType);
					 workOrderRealign.setFromUser(userId);
					 workOrderRealign.setAlignTime(new Date());
					 workOrderRealign.setRealignReason(workOrder.getSolveRemark());
					 String json=JSONObject.toJSONString(workOrder);
					 workOrderRealign.setJson(json);
					 workOrderRealign.setBeginTime(beginTime);
					 workOrderRealignService.save(workOrderRealign);
					 sendMsgType=WorkConst.MSG_TYPE_PAIDAN;
					 //状态转到派单
					 workOrder.setStatus(WorkConst.ORDER_STATUS_ALIGN);
					 workOrder.setToAlignTime(new Date());
				 }
			 }
			 if(WorkConst.ORDER_STATUS_SCORE.equals(status)){
				 //评价
				 if(alignType.equals(WorkConst.PAIDAN_STATUS_PINGJIA)){
					 workOrder.setScoreUser(userId);
					 workOrder.setStatus(WorkConst.ORDER_STATUS_DONE);
					 workOrder.setScoreTime(new Date());
					 sendMsgType=WorkConst.MSG_TYPE_WANCHENG;
					 WorkOrderRealign workOrderRealign=new WorkOrderRealign();
					 workOrderRealign.setWorkOrder(workOrder.getId());
					 workOrderRealign.setType(alignType);
					 workOrderRealign.setFromUser(userId);
					 workOrderRealign.setAlignTime(new Date());
					 workOrderRealign.setRealignReason(workOrder.getIsSolveVerifyRemark());
					 String json=JSONObject.toJSONString(workOrder);
					 workOrderRealign.setJson(json);
					 workOrderRealign.setBeginTime(beginTime);
					 workOrderRealignService.save(workOrderRealign);
				 }
				 else if(alignType.equals(WorkConst.PAIDAN_STATUS_TUIHUI)){
					 //退单
					 WorkOrderRealign workOrderRealign=new WorkOrderRealign();
					 workOrderRealign.setWorkOrder(workOrder.getId());
					 workOrderRealign.setType(alignType);
					 workOrderRealign.setFromUser(userId);
					 workOrderRealign.setAlignTime(new Date());
					 workOrderRealign.setRealignReason(workOrder.getIsSolveVerifyRemark());
					 String json=JSONObject.toJSONString(workOrder);
					 workOrderRealign.setJson(json);
					 workOrderRealign.setBeginTime(beginTime);
					 workOrderRealignService.save(workOrderRealign);
					 sendMsgType=WorkConst.MSG_TYPE_PAIDAN;				 //状态转到派单
					 workOrder.setStatus(WorkConst.ORDER_STATUS_ALIGN);
					 workOrder.setToAlignTime(new Date());
				 }
			 }
			}
			 workOrderService.updateById(workOrder);
		 
		 
		 new SendMsgThread(workOrder.getId(),sendMsgType,domain,agentId,appKey,appSecret,workOrderService,sysBaseAPI,workOrderTypeUserService,dingMsgService,workOrderTypeDiyColumnService,
				 0
				 ,WorkConst.DING_MSG_TYPE_COMMON,1).start();

		 if(WorkConst.baozhangType_kehu.equals(workOrder.getBaozhangType())){
		 	if(msgMark.equals(WorkConst.sms_template_jiedan)){
				 HashMap<String,String> param=new HashMap<>();
				 param.put("code",workOrder.getId()+"_1");
				 AliSms.sendSms(WorkConst.sms_template_jiedan,JSON.toJSONString(param),workOrder.getContactPhone());
			 }
			 if(msgMark.equals(WorkConst.sms_template_wancheng)){
				 HashMap<String,String> param=new HashMap<>();
				 param.put("code",workOrder.getId()+"_1");
				 AliSms.sendSms(WorkConst.sms_template_wancheng,JSON.toJSONString(param),workOrder.getContactPhone());
			 }
		 }
		 return Result.ok("办理成功!");
	 }
	 /**
	  *   按项目类型统计
	  * @return
	  */
	 //@AutoLog(value = "按项目类型统计")
	 @ApiOperation(value="按项目类型统计", notes="按项目类型统计")
	 @GetMapping(value = "/countByType")
	 public Result<?> countByType(HttpServletRequest req) {
	 	String alignDay=req.getParameter("alignDay");
		 String alignMon=req.getParameter("alignMon");
		 String alignYear=req.getParameter("alignYear");
		 List<HashMap<String,Object>> list=workOrderMapper.countByType(alignDay,alignMon,alignYear);
		 return Result.ok(list);
	 }
	 public static Long add(Long a,Long b){
	 	if(null==a){
			a= new Long(0);
		}
		 if(null==b){
			 b= new Long(0);
		 }
		 return a+b;
	 }
	 /**
	  *   按项目类型统计
	  * @return
	  */
	 //@AutoLog(value = "按状态统计")
	 @ApiOperation(value="按状态统计", notes="按状态统计")
	 @GetMapping(value = "/countByStatus")
	 public Result<?> countByStatus(HttpServletRequest req) {
	 	String zaituMark=req.getParameter("zaituMark");
		 String userType=req.getParameter("userType");
		 String userId=null;
		 if("mine".equals(userType)){
			 String username = JwtUtil.getUserNameByToken(req);
			 final List<String> types=new ArrayList<String>();
			 if(!StringUtil.isNullOrEmpty(username)){
				 LoginUser loginUser=sysBaseAPI.getUserByName(username);
				 if(null!=loginUser){
					 userId=loginUser.getId();
				 }
			 }else{
				 return  Result.error("用户不存在");
			 }
		 }
		 List<HashMap<String,Object>> list=workOrderMapper.countByStatus(zaituMark,userId);
		 return Result.ok(list);
	 }
	 @ApiOperation(value="生成报障二维码", notes="生成报障二维码")
	 @GetMapping(value = "/generateQRCodeImage")
	 public Result<?> generateQRCodeImage(
			 HttpServletRequest req) {
//		 String tenantId=req.getParameter("tenantId");
//		 if(!StringUtil.isNullOrEmpty(tenantId)){
			 try{

//				 Tenant tenant=tenantService.getById(tenantId);
//				 if(null!=tenant){
					 QRCodeGenerator.generateQRCodeImage(domain+ File.separator+"select_type_noauth",500,500,upLoadPath+File.separator+ "zizhubaozhang_qrcode.png");
//					 tenant.setBaozhangQrcode(tenantId+"_qrcode.png");
//					 tenantService.updateById(tenant);
//				 }
			 }catch (Exception e){
				 log.error(e.getMessage(),e);
			 }
//		 }

		 return Result.ok();
	 }
	 /**
	  *   按人统计
	  * @return
	  */
	 //@AutoLog(value = "按人统计")
	 @ApiOperation(value="按人统计", notes="按人统计")
	 @GetMapping(value = "/countByUser")
	 public Result<?> countByUser(HttpServletRequest req) {
		 String arignDay=req.getParameter("alignDay");
		 String arignMon=req.getParameter("alignMon");
		 String arignYear=req.getParameter("alignYear");
		 String type=req.getParameter("type");
//		 if (StringUtil.isNullOrEmpty(type)) {
//
//			 return Result.error("type不能为空");
//		 }
		 List<HashMap<String,Object>> list=workOrderMapper.countByUser(type,arignDay,arignMon,arignYear);
		 List<Team> teams=teamService.list();
		 List<JSONObject> users=sysBaseAPI.getAllUserNames();
		 List<TeamUser> teamUsers=teamUserService.list();

		 List<JSONObject> rel=new ArrayList<JSONObject>();
		 if(null!=teams&&teams.size()>0){
			 for(Team team:teams){
			 	JSONObject jsonObject=new JSONObject();
			 	jsonObject.put("id",team.getId());
			 	jsonObject.put("name",team.getName());
				 List<JSONObject> subUsers=new ArrayList<JSONObject>();
				 Long count_all=new Long(0);
				 Long count_solve=new Long(0);
				 Long count_solve_verify=new Long(0);
				 Long count_dupli60=new Long(0);
				 if(null!=teamUsers&&teamUsers.size()>0) {

					 for (TeamUser teamUser : teamUsers) {
					 	if(team.getId().equals(teamUser.getTeam())){
					 		JSONObject userJson=new JSONObject();
							userJson.put("id",teamUser.getUser());
							Optional<JSONObject>  selectedUsers=users.stream().filter(item->(teamUser.getUser().equals(null!=item.get("value")?(String)item.get("value"):""))).findFirst();
							if(null!=selectedUsers && selectedUsers.isPresent()) {
								userJson.put("userName", (String) selectedUsers.get().get("label"));
							}
//							t1.to_user as user,t1.count_all,t2.count_solve,t3.count_solve_verify,t4.count_dupli60
							Optional<HashMap<String,Object>>  selectedCount=list.stream().filter(item->(teamUser.getUser().equals(null!=item.get("user")?(String)item.get("user"):""))).findFirst();
							if(null!=selectedCount && selectedCount.isPresent()) {
								userJson.put("count_all",  selectedCount.get().get("count_all"));
								userJson.put("count_solve", selectedCount.get().get("count_solve"));
								userJson.put("count_solve_verify",  selectedCount.get().get("count_solve_verify"));
								userJson.put("count_dupli60", selectedCount.get().get("count_dupli60"));
								count_all=this.add(count_all,null!=selectedCount.get().get("count_all")?(Long)selectedCount.get().get("count_all"):null);
								count_solve=this.add(count_solve,null!=selectedCount.get().get("count_solve")?(Long)selectedCount.get().get("count_solve"):null);
								count_solve_verify=this.add(count_solve_verify,null!=selectedCount.get().get("count_solve_verify")?(Long)selectedCount.get().get("count_solve_verify"):null);
								count_dupli60=this.add(count_dupli60,null!=selectedCount.get().get("count_dupli60")?(Long)selectedCount.get().get("count_dupli60"):null);

							}
							subUsers.add(userJson);
						}
					 }
				 }
				 jsonObject.put("users",subUsers);
				 jsonObject.put("count_all",count_all);
				 jsonObject.put("count_solve",count_solve);
				 jsonObject.put("count_solve_verify",count_solve_verify);
				 jsonObject.put("count_dupli60",count_dupli60);
				 rel.add(jsonObject);

			 }
		 }

		 return Result.ok(rel);
	 }
	 /**
	  *   按人统计
	  * @return
	  */
	 //@AutoLog(value = "当前用户待办")
	 @ApiOperation(value="当前用户待办", notes="当前用户待办")
	 @GetMapping(value = "/countCurUser")
	 public Result<?> countCurUser(HttpServletRequest req) {
		 String username = JwtUtil.getUserNameByToken(req);

		 String userId="";
		 final List<String> types=new ArrayList<String>();
		 if(!StringUtil.isNullOrEmpty(username)){
			 LoginUser loginUser=sysBaseAPI.getUserByName(username);
			 if(null!=loginUser){
				 userId=loginUser.getId();
				 QueryWrapper<WorkOrderTypeUser> queryWrapperU=new QueryWrapper<WorkOrderTypeUser>();
				 queryWrapperU.eq("align_user",userId);
				 List<WorkOrderTypeUser> workOrderTypeUsers=workOrderTypeUserService.list(queryWrapperU);

				 types.add("empty");
				 if(null!=workOrderTypeUsers&&workOrderTypeUsers.size()>0){
					 workOrderTypeUsers.stream().forEach(item->
							 types.add(item.getWorkOrderType()));
				 }

			 }
		 }else{
			 return  Result.error("用户不存在");
		 }
		 HashMap<String,Object> rel=new HashMap<String,Object>();
		 int daiban=0;
		 int daipaidan=0;
		 int daipingjia=0;
		 int daijiedan=0;
		 QueryWrapper<WorkOrder> queryWrapperPaidan = new QueryWrapper<WorkOrder>();
		 queryWrapperPaidan.eq("status",WorkConst.ORDER_STATUS_ALIGN);
		 queryWrapperPaidan.in("type",types);
		 queryWrapperPaidan.select("id");
		 daipaidan = workOrderService.count( queryWrapperPaidan);


		 QueryWrapper<WorkOrder> queryWrapperChuli = new QueryWrapper<WorkOrder>();
		 queryWrapperChuli.eq("status", WorkConst.ORDER_STATUS_HANDLE);
		 queryWrapperChuli.eq("to_user",userId);
		 queryWrapperChuli.select("id");
		 daiban = workOrderService.count( queryWrapperChuli);

		 QueryWrapper<WorkOrder> queryWrapperGet = new QueryWrapper<WorkOrder>();
		 queryWrapperGet.eq("status", WorkConst.ORDER_STATUS_GET);
		 queryWrapperGet.eq("to_user",userId);
		 queryWrapperGet.select("id");
		 daijiedan = workOrderService.count( queryWrapperGet);

		 QueryWrapper<WorkOrder> queryWrapperPingjia = new QueryWrapper<WorkOrder>();
		 queryWrapperPingjia.eq("status",WorkConst.ORDER_STATUS_SCORE);
			 queryWrapperPingjia.eq("from_user",userId);
		 queryWrapperPingjia.select("id");
		 daipingjia = workOrderService.count( queryWrapperPingjia);

		 rel.put("daiban",daiban);
		 rel.put("daipaidan",daipaidan);
		 rel.put("daipingjia",daipingjia);
		 rel.put("daijiedan",daijiedan);

		 return Result.ok(rel);
	 }
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "工单-通过id删除")
	@ApiOperation(value="工单-通过id删除", notes="工单-通过id删除")
	@GetMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		workOrderService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	//@AutoLog(value = "工单-批量删除")
	@ApiOperation(value="工单-批量删除", notes="工单-批量删除")
	@GetMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.workOrderService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "工单-通过id查询")
	@ApiOperation(value="工单-通过id查询", notes="工单-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		WorkOrder workOrder = workOrderService.getById(id);
		if(workOrder==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(workOrder);
	}
	 /**
	  * 对象是否为数组对象
	  *
	  * @param obj 对象
	  * @return 是否为数组对象，如果为{@code null} 返回false
	  */
	 public static boolean isArray(Object obj) {
		 if (null == obj) {
//            throw new NullPointerException("Object check for isArray is null");
			 return false;
		 }
//        反射 获得类型
		 return obj.getClass().isArray();
	 }
	 /**
	  * 通过id查询
	  *
	  * @param id
	  * @return
	  */
	 //@AutoLog(value = "工单-通过id查询含翻译")
	 @ApiOperation(value="工单-通过id查询", notes="工单-通过id查询")
	 @GetMapping(value = "/queryByIdAll")
	 public Result<?> queryByIdAll(@RequestParam(name="id",required=true) String id,HttpServletRequest req) {
		 WorkOrder workOrder = workOrderService.getById(id);
		 String canHandle="";
		 if(workOrder==null) {
			 return Result.error("未找到对应数据");
		 }
		 String userId="";
		 String username =null;
		 try{
			 username=JwtUtil.getUserNameByToken(req);
		 }catch (Exception e){
		 	log.error(e.getMessage(),e);
		 }
		 final List<String> types=new ArrayList<String>();
		 if(!StringUtil.isNullOrEmpty(username)) {
			 LoginUser loginUser = sysBaseAPI.getUserByName(username);
			 if (null != loginUser) {
				 userId = loginUser.getId();
			 }
			 List<String> roles=sysBaseAPI.getRolesByUsername(username);
			 String roleMark="";
			 if(null!=roles&&roles.size()>0) {
				 for (String r : roles) {
					 roleMark += r + ",";
				 }
			 }
			 if(roleMark.indexOf("admin")>=0||roleMark.indexOf("guanliyuan")>=0||roleMark.indexOf("bigscreen")>=0){
				 //管理员或者有统计分析权限
				 //放行
			 }else{
				 //queryWrapper.apply(StrUtil.isNotBlank(userType),
				 //						"(from_user = '" + userId + "' or to_user = '" + userId +"' or align_user = '" + userId +"' or score_user = '" + userId+"')");
				 //判断是否是本人经办数据
				 if(userId.equals(workOrder.getFromUser())||userId.equals(workOrder.getToUser())
						 ||userId.equals(workOrder.getAlignUser())||userId.equals(workOrder.getScoreUser())){
					 //放行
				 }else{
					 return Result.error("用户未授权");
				 }
			 }
		 }else{
//				 return Result.error("用户未授权，请检查");
		 }

		 if(StringUtil.isNullOrEmpty(workOrder.getType())){
			 return Result.error("数据异常");
		 }
//		 List<HashMap<String,Object>> rel=workOrderMapper.countDaiban(tenantId);
		 QueryWrapper<WorkOrderTypeTeam> workOrderTypeTeamQueryWrapper= new QueryWrapper<WorkOrderTypeTeam>();
		 workOrderTypeTeamQueryWrapper.eq("work_order_type",workOrder.getType());
		 List <WorkOrderTypeTeam> workOrderTypeTeams=workOrderTypeTeamService.list(workOrderTypeTeamQueryWrapper);
		 List<JSONObject> temp=new ArrayList<JSONObject>();
		 String teamId="";
		 if(null!=workOrderTypeTeams&&workOrderTypeTeams.size()==1) {
			 teamId = workOrderTypeTeams.get(0).getTeam();
		 }
		if(workOrder.getStatus().equals(WorkConst.ORDER_STATUS_ALIGN)){
			workOrder.setAlignUser(null);
			workOrder.setAlignTime(null);
			workOrder.setToUser(null);
			workOrder.setQuickHandleMark(null);
			workOrder.setQuickHandleRemark(null);
		}
		 if(workOrder.getStatus().equals(WorkConst.ORDER_STATUS_HANDLE)){
			 workOrder.setSolveTime(null);
			 workOrder.setSolveRemark(null);
			 workOrder.setSolvePic(null);
			 workOrder.setIsSolve(null);
			 workOrder.setChuliExt1(null);
			 workOrder.setChuliExt2(null);
			 workOrder.setChuliExt3(null);
			 workOrder.setChuliExt4(null);
			 workOrder.setChuliExt5(null);
			 workOrder.setChuliExt6(null);
			 workOrder.setChuliExt7(null);
			 workOrder.setChuliExt8(null);
			 workOrder.setChuliExt9(null);
			 workOrder.setChuliExt10(null);
		 }
		 if(workOrder.getStatus().equals(WorkConst.ORDER_STATUS_GET)){
			 workOrder.setGetTime(null);
		 }
		 if(workOrder.getStatus().equals(WorkConst.ORDER_STATUS_SCORE)){
			 workOrder.setScore(null);
			 workOrder.setScoreTime(null);
			 workOrder.setScoreUserOpinion(null);
			 workOrder.setScoreUser(null);
			 workOrder.setScoreUserSatisfied(null);
			 workOrder.setPingjiaExt1(null);
			 workOrder.setPingjiaExt2(null);
			 workOrder.setPingjiaExt3(null);
			 workOrder.setPingjiaExt4(null);
			 workOrder.setPingjiaExt5(null);
			 workOrder.setPingjiaExt6(null);
			 workOrder.setPingjiaExt7(null);
			 workOrder.setPingjiaExt8(null);
			 workOrder.setPingjiaExt9(null);
			 workOrder.setPingjiaExt10(null);
		 }
		 JSONObject relJson=sysBaseAPI.parseDictTextOjb(workOrder);
		 String chuliren="";
		 String paidanrenMark="";
		 List<WorkOrderTypeUser> workOrderTypeUsers=null;
		 if(null==workOrderTypeUsers){
			 QueryWrapper<WorkOrderTypeUser> queryWrapperU=new QueryWrapper<WorkOrderTypeUser>();
			 queryWrapperU.eq("work_order_type",workOrder.getType());
			 workOrderTypeUsers=workOrderTypeUserService.list(queryWrapperU);
		 }
		 if(StringUtil.isNullOrEmpty(paidanrenMark)){
			 if(!StringUtil.isNullOrEmpty(userId)){
				 if(null!=workOrderTypeUsers==workOrderTypeUsers.size()>0){
					 for(WorkOrderTypeUser workOrderTypeUser:workOrderTypeUsers){
						 if(userId.equals(workOrderTypeUser.getAlignUser())){
							 paidanrenMark="1";
						 }
					 }

				 }
			 }
		 }
		 if(WorkConst.ORDER_STATUS_ALIGN.equals(workOrder.getStatus())){
			 if(!StringUtil.isNullOrEmpty(userId)){
			 	

				 if(null!=workOrderTypeUsers==workOrderTypeUsers.size()>0){
				 	for(WorkOrderTypeUser workOrderTypeUser:workOrderTypeUsers){
				 		if(userId.equals(workOrderTypeUser.getAlignUser())){
							canHandle="1";
						}
				 		if(!StringUtil.isNullOrEmpty(workOrderTypeUser.getAlignUser())){
							LoginUser loginUser=sysBaseAPI.getUserById(workOrderTypeUser.getAlignUser());
							if(null!=loginUser){
								chuliren+=loginUser.getRealname()+",";
							}
						}
					}
				 	if(chuliren.endsWith(",")){
				 		chuliren=chuliren.substring(0,chuliren.length()-1);
					}
				}
			 }
		 }else if(WorkConst.ORDER_STATUS_GET.equals(workOrder.getStatus())){
			 if(!StringUtil.isNullOrEmpty(userId)&&userId.equals(workOrder.getToUser())){
				 canHandle="1";

			 }
			 chuliren=relJson.getString("toUser_dictText");
		 }
		 else if(WorkConst.ORDER_STATUS_HANDLE.equals(workOrder.getStatus())){
			 if(!StringUtil.isNullOrEmpty(userId)&&userId.equals(workOrder.getToUser())){
				 canHandle="1";
			 }
			 chuliren=relJson.getString("toUser_dictText");
		 }
		 else if(WorkConst.ORDER_STATUS_SCORE.equals(workOrder.getStatus())){
			 if(!StringUtil.isNullOrEmpty(userId)&&userId.equals(workOrder.getFromUser())){
				 canHandle="1";
			 }
			 chuliren=relJson.getString("fromUser_dictText");
		 }
		 if(!StringUtil.isNullOrEmpty(workOrder.getFromUser())){
			 LoginUser loginUser=sysBaseAPI.getUserById(workOrder.getFromUser());
			 if(null!=loginUser){
				 relJson.put("fromUserPhone",loginUser.getPhone());
			 }
		 }
		 if(WorkConst.ORDER_STATUS_GET.equals(workOrder.getStatus())||
				 WorkConst.ORDER_STATUS_HANDLE.equals(workOrder.getStatus())||
				 WorkConst.ORDER_STATUS_SCORE.equals(workOrder.getStatus())){
			 if(null==workOrderTypeUsers){
				 QueryWrapper<WorkOrderTypeUser> queryWrapperU=new QueryWrapper<WorkOrderTypeUser>();
				 queryWrapperU.eq("work_order_type",workOrder.getType());
				 workOrderTypeUsers=workOrderTypeUserService.list(queryWrapperU);
			 }
			 if(!StringUtil.isNullOrEmpty(userId)&&null!=workOrderTypeUsers && workOrderTypeUsers.size()>0){

				 if(null!=workOrderTypeUsers==workOrderTypeUsers.size()>0){
					 for(WorkOrderTypeUser workOrderTypeUser:workOrderTypeUsers){
						 if(userId.equals(workOrderTypeUser.getAlignUser())){
							 canHandle="1";
							 break;
						 }
					 }
				 }
			 }
		 }
		 //扩展字段展示
		 QueryWrapper<WorkOrderTypeDiyColumn> workOrderTypeDiyColumnQueryWrapper=new QueryWrapper<WorkOrderTypeDiyColumn>();
		 workOrderTypeDiyColumnQueryWrapper.eq("work_order_type",workOrder.getType());
		 workOrderTypeDiyColumnQueryWrapper.eq("is_show","1");
		 workOrderTypeDiyColumnQueryWrapper.orderByAsc("order_num");
		 List<WorkOrderTypeDiyColumn> workOrderTypeDiyColumns=workOrderTypeDiyColumnService.list(workOrderTypeDiyColumnQueryWrapper);
		 List<HashMap<String,Object>> baozhangScope=new ArrayList<HashMap<String,Object>>();
		 List<HashMap<String,Object>> chuliScope=new ArrayList<HashMap<String,Object>>();
		 List<HashMap<String,Object>> pingjiaScope=new ArrayList<HashMap<String,Object>>();
		 if(null!=workOrderTypeDiyColumns&&workOrderTypeDiyColumns.size()>0){
		 	for(WorkOrderTypeDiyColumn workOrderTypeDiyColumn:workOrderTypeDiyColumns){
		 		String scope=workOrderTypeDiyColumn.getScope();
		 		String workOrderType=workOrderTypeDiyColumn.getWorkOrderType();
		 		String columnCode=workOrderTypeDiyColumn.getColumnCode();
		 		String componentType=workOrderTypeDiyColumn.getComponentType();
		 		String columnName=workOrderTypeDiyColumn.getColumnName();
		 		String autoInputFormColumn=workOrderTypeDiyColumn.getAutoInputFromColumn();
		 		String duplicatedMark=workOrderTypeDiyColumn.getDuplicatedMark();
		 		String multiple=workOrderTypeDiyColumn.getMultiple();
		 		String dictJson=workOrderTypeDiyColumn.getDictJson();
				String val_dictText="";
				HashMap<String,Object> column=new HashMap<>();
				if(workOrder.getType().equals(workOrderType)){
					if(null!=relJson.get(autoInputFormColumn)){
						if(WorkConst.DIY_SCOPE_CHULI.equals(scope)&&WorkConst.ORDER_STATUS_HANDLE.equals(workOrder.getStatus())){
							relJson.put(columnCode,relJson.get(autoInputFormColumn));
						}
						if(WorkConst.DIY_SCOPE_PINGJIA.equals(scope)&&WorkConst.ORDER_STATUS_SCORE.equals(workOrder.getStatus())){
							relJson.put(columnCode,relJson.get(autoInputFormColumn));
						}
					}
					Object val=relJson.get(columnCode);
						if(null!=val){
							if(WorkConst.COMPONENT_TYPE_WENBEN.equals(componentType)||
									WorkConst.COMPONENT_TYPE_DUOHANGWENBEN.equals(componentType)||
									WorkConst.COMPONENT_TYPE_RIQI.equals(componentType)||
									WorkConst.COMPONENT_TYPE_SHIJIAN.equals(componentType)
							){
								val_dictText=val.toString();
							}
							if(WorkConst.COMPONENT_TYPE_TUPIAN.equals(componentType)||
									WorkConst.COMPONENT_TYPE_WENJIAN.equals(componentType)){
								String[] arr=val.toString().split(",");
								for(String str:arr){
									if(!StringUtil.isNullOrEmpty(str)){
										val_dictText+="/jeecgboot/lst-boot/sys/common/static/"+str;
									}
								}
								if(val_dictText.endsWith(",")){
									val_dictText=val_dictText.substring(0,val_dictText.length()-1);
								}
							}
							if(WorkConst.COMPONENT_TYPE_XIALAKUANG.equals(componentType)){
								String[] strs=null;
								if(isArray(val)){
									strs=(String[])val;
								}else{
									strs=val.toString().split(",");
								}

								JSONArray jsonArray=JSONArray.parseArray(dictJson);
								if(null!=jsonArray&&jsonArray.size()>0){
									if(null!=strs&&strs.length>0){
										for(String str:strs){
											for(int i=0;i<jsonArray.size();i++){
												JSONObject jsonObject=(JSONObject) jsonArray.get(i);
												if(null!=jsonObject){

													if(null!=jsonObject.get("value")&&null!=jsonObject.get("label")){
														if(str.equals((String) jsonObject.get("value"))){
															String text=(String)jsonObject.get("label");
															val_dictText+=text+",";
															break;
														}
													}
												}
											}
										}
									}

								}
								if(val_dictText.endsWith(",")){
									val_dictText=val_dictText.substring(0,val_dictText.length()-1);
								}
							}
							column.put("value",val);
							column.put("text",val_dictText);
							if(WorkConst.COMPONENT_TYPE_XIALAKUANG.equals(componentType)&&"1".equals(multiple)){
								if(isArray(val)) {
									column.put("value",(String[]) val);
									relJson.put(columnCode,(String[]) val);
								}else{
									String[] arr=val.toString().split(",");
									column.put("value",arr);
									relJson.put(columnCode,arr);

								}
							}

					}
					column.put("columnCode",columnCode);
					column.put("columnName",columnName);

					WorkOrderTypeDiyColumnView workOrderTypeDiyColumnView = new WorkOrderTypeDiyColumnView();
					BeanUtils.copyProperties(workOrderTypeDiyColumn, workOrderTypeDiyColumnView);
					JSONArray jsonArray = JSONArray.parseArray(workOrderTypeDiyColumn.getDictJson());
					workOrderTypeDiyColumnView.setDictArr(jsonArray);
					column.put("diy",workOrderTypeDiyColumnView);
					if(WorkConst.DIY_SCOPE_BAOZHANG.equals(scope)){
						baozhangScope.add(column);
					}else if(WorkConst.DIY_SCOPE_CHULI.equals(scope)){
						chuliScope.add(column);
					}else if(WorkConst.DIY_SCOPE_PINGJIA.equals(scope)){
						pingjiaScope.add(column);
					}
				}


			}
		 }
		 if(WorkConst.ORDER_STATUS_HANDLE.equals(workOrder.getStatus())){

		 }
		 BigDecimal overtimeHoursHandle=null;
		 WorkOrderType workOrderType=workOrderTypeService.getById(workOrder.getType());
		 if(null!=workOrderType){
			 overtimeHoursHandle=workOrderType.getOvertimeHoursHandle();
		 	if(null!=overtimeHoursHandle){
				Date createTime=workOrder.getCreateTime();
				Calendar calendar=Calendar.getInstance();
				calendar.setTime(createTime);
			}
		 }
		 relJson.put("overtimeHoursHandle",overtimeHoursHandle);
		 relJson.put("teamId",teamId);
		 relJson.put("baozhangScope",baozhangScope);
		 relJson.put("chuliScope",chuliScope);
		 relJson.put("pingjiaScope",pingjiaScope);
		 relJson.put("canHandle",canHandle);

		 relJson.put("chuliren",chuliren);
		 relJson.put("paidanrenMark",paidanrenMark);
		 String lishi_zong="";
		 String lishi_paidan="";
		 String lishi_jiedan="";
		 String lishi_chuli="";
		 String lishi_queren="";
		 lishi_zong= SendMsgTask.timeConsuming(workOrder.getCreateTime(),
				 workOrder.getStatus().compareTo(WorkConst.ORDER_STATUS_DONE)==0?workOrder.getScoreTime():new Date());
		 lishi_paidan= SendMsgTask.timeConsuming(null!=workOrder.getToAlignTime()?workOrder.getToAlignTime():workOrder.getCreateTime(),
				 workOrder.getStatus().compareTo(WorkConst.ORDER_STATUS_ALIGN)>0?workOrder.getAlignTime():new Date());
		 lishi_jiedan= SendMsgTask.timeConsuming(workOrder.getAlignTime(),
				 workOrder.getStatus().compareTo(WorkConst.ORDER_STATUS_GET)>0?workOrder.getGetTime():new Date());
		 lishi_chuli= SendMsgTask.timeConsuming(workOrder.getGetTime(),
				 workOrder.getStatus().compareTo(WorkConst.ORDER_STATUS_HANDLE)>0?workOrder.getSolveTime():new Date());
		 lishi_queren= SendMsgTask.timeConsuming(workOrder.getSolveTime(),
				 workOrder.getStatus().compareTo(WorkConst.ORDER_STATUS_SCORE)>0?workOrder.getScoreTime():new Date());
		 relJson.put("lishi_zong",lishi_zong);
		 relJson.put("lishi_paidan",lishi_paidan);
		 relJson.put("lishi_jiedan",lishi_jiedan);
		 relJson.put("lishi_chuli",lishi_chuli);
		 relJson.put("lishi_queren",lishi_queren);
		 return Result.ok(relJson);
	 }

	 /**
	  * 查询报障diy配置
	  *
//	  * @param id
	  * @return
	  */
	 //@AutoLog(value = "工单-通过id查询含翻译")
	 @ApiOperation(value="查询报障diy配置", notes="查询报障diy配置")
	 @GetMapping(value = "/queryBaozhangDiy")
	 public Result<?> queryBaozhangDiy(@RequestParam(name="workOrderType",required=true) String workOrderType) {
		 //扩展字段展示
		 QueryWrapper<WorkOrderTypeDiyColumn> workOrderTypeDiyColumnQueryWrapper=new QueryWrapper<WorkOrderTypeDiyColumn>();
		 workOrderTypeDiyColumnQueryWrapper.eq("work_order_type",workOrderType);
		 workOrderTypeDiyColumnQueryWrapper.eq("is_show","1");
		 workOrderTypeDiyColumnQueryWrapper.eq("scope",WorkConst.DIY_SCOPE_BAOZHANG);
		 workOrderTypeDiyColumnQueryWrapper.orderByAsc("order_num");
		 List<WorkOrderTypeDiyColumn> workOrderTypeDiyColumns=workOrderTypeDiyColumnService.list(workOrderTypeDiyColumnQueryWrapper);
		 List<HashMap<String,Object>> baozhangScope=new ArrayList<HashMap<String,Object>>();
		 List<HashMap<String,Object>> chuliScope=new ArrayList<HashMap<String,Object>>();
		 List<HashMap<String,Object>> pingjiaScope=new ArrayList<HashMap<String,Object>>();
		 List<WorkOrderTypeDiyColumnView> rel=new ArrayList<WorkOrderTypeDiyColumnView>();
		 if(null!=workOrderTypeDiyColumns&&workOrderTypeDiyColumns.size()>0) {
			 for (WorkOrderTypeDiyColumn workOrderTypeDiyColumn : workOrderTypeDiyColumns) {
				 WorkOrderTypeDiyColumnView workOrderTypeDiyColumnView = new WorkOrderTypeDiyColumnView();
				 BeanUtils.copyProperties(workOrderTypeDiyColumn, workOrderTypeDiyColumnView);

				 String dictJson = workOrderTypeDiyColumn.getDictJson();
				 JSONArray jsonArray = JSONArray.parseArray(dictJson);
				 workOrderTypeDiyColumnView.setDictArr(jsonArray);
				 rel.add(workOrderTypeDiyColumnView);
			 }
		 }
		 return Result.ok(rel);
	 }
	 @ApiOperation(value="查询报障diy配置", notes="查询报障diy配置")
	 @GetMapping(value = "/queryBaozhangDiyAll")
	 public Result<?> queryBaozhangDiy() {
		 //扩展字段展示
		 QueryWrapper<WorkOrderTypeDiyColumn> workOrderTypeDiyColumnQueryWrapper=new QueryWrapper<WorkOrderTypeDiyColumn>();
		 workOrderTypeDiyColumnQueryWrapper.eq("is_show","1");
		 workOrderTypeDiyColumnQueryWrapper.eq("scope",WorkConst.DIY_SCOPE_BAOZHANG);
		 workOrderTypeDiyColumnQueryWrapper.orderByAsc("order_num");
		 List<WorkOrderTypeDiyColumn> workOrderTypeDiyColumns=workOrderTypeDiyColumnService.list(workOrderTypeDiyColumnQueryWrapper);
		 HashMap<String,Object> rel=new HashMap<String,Object>();


		 if(null!=workOrderTypeDiyColumns&&workOrderTypeDiyColumns.size()>0) {
			 for (WorkOrderTypeDiyColumn workOrderTypeDiyColumn : workOrderTypeDiyColumns) {
				 WorkOrderTypeDiyColumnView workOrderTypeDiyColumnView = new WorkOrderTypeDiyColumnView();
				 BeanUtils.copyProperties(workOrderTypeDiyColumn, workOrderTypeDiyColumnView);

				 String dictJson = workOrderTypeDiyColumn.getDictJson();
				 JSONArray jsonArray = JSONArray.parseArray(dictJson);
				 workOrderTypeDiyColumnView.setDictArr(jsonArray);
				 List<WorkOrderTypeDiyColumnView> tmp=new ArrayList<WorkOrderTypeDiyColumnView>();
				 if(null!=rel.get(workOrderTypeDiyColumn.getWorkOrderType())){
					 tmp=(List<WorkOrderTypeDiyColumnView>)rel.get(workOrderTypeDiyColumn.getWorkOrderType());
				 }else{
					 tmp=new ArrayList<WorkOrderTypeDiyColumnView>();
					 rel.put(workOrderTypeDiyColumn.getWorkOrderType(),tmp);
				 }
				 tmp.add(workOrderTypeDiyColumnView);

			 }
		 }
		 return Result.ok(rel);
	 }
    /**
    * 导出excel
    *
    * @param request
    * @param workOrder
    */
    @RequestMapping(value = "/exportXls")
    public String exportXls(HttpServletRequest request, WorkOrder workOrder,HttpServletResponse response) {
//        return super.exportXls(request, workOrder, WorkOrder.class, "工单");
		QueryWrapper<WorkOrder> queryWrapper = QueryGenerator.initQueryWrapper(workOrder, request.getParameterMap());
		List<WorkOrder> pageList = workOrderService.list(queryWrapper);
		HSSFWorkbook workbook = workOrderService.exportExcel(pageList);

		 ExcelImpl excleImpl = new ExcelImpl();
		 try{
			 ServletOutputStream out = response.getOutputStream();
			 excleImpl.export(workbook, out);
		 }catch (Exception e){
			log.error(e.getMessage(),e);
		 }
		return "success";
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WorkOrder.class);
    }

}
