package com.lst.work.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.lst.dingtalk.Sample;
import com.lst.work.entity.SysSetting;
import com.lst.work.service.ISysSettingService;
import io.netty.util.internal.StringUtil;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import com.lst.work.entity.TeamUser;
import com.lst.work.service.ITeamUserService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 团队用户关系
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Api(tags="团队用户关系")
@RestController
@RequestMapping("/work/teamUser")
@Slf4j
public class TeamUserController extends JeecgController<TeamUser, ITeamUserService> {
	@Autowired
	private ITeamUserService teamUserService;
	@Autowired
	private ISysSettingService sysSettingService;

	/**
	 * 分页列表查询
	 *
	 * @param teamUser
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "团队用户关系-分页列表查询")
	@ApiOperation(value="团队用户关系-分页列表查询", notes="团队用户关系-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(TeamUser teamUser,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<TeamUser> queryWrapper = QueryGenerator.initQueryWrapper(teamUser, req.getParameterMap());
		Page<TeamUser> page = new Page<TeamUser>(pageNo, pageSize);
		IPage<TeamUser> pageList = teamUserService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	 /**
	  * 同步钉钉通讯录
	  *
	  * @param teamUser
	  * @param pageNo
	  * @param pageSize
	  * @param req
	  * @return
	  */
	 //@AutoLog(value = "同步钉钉通讯录")
	 @ApiOperation(value="同步钉钉通讯录", notes="同步钉钉通讯录")
	 @GetMapping(value = "/synchUserList")
	 public Result<?> synchUserList(TeamUser teamUser,
									@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
	 	String rel="操作成功";
		 String domain="";
		 String agentId="";
		 String appKey="";
		 String appSecret="";

		 List<SysSetting> sysSettings=sysSettingService.list();
		 if(null!=sysSettings&&sysSettings.size()>0){
			 for(SysSetting sysSetting:sysSettings){
				 if("1".equals(sysSetting.getIsValid())){
					 if("domain".equals(sysSetting.getCode())){
						 domain=sysSetting.getValue();
					 }
					 if("agentId".equals(sysSetting.getCode())){
						 agentId=sysSetting.getValue();
					 }
					 if("appKey".equals(sysSetting.getCode())){
						 appKey=sysSetting.getValue();
					 }
					 if("appSecret".equals(sysSetting.getCode())){
						 appSecret=sysSetting.getValue();
					 }
				 }
			 }
		 }else{
			 log.error("数据异常");
		 }
	 	try {
			String accessToken = Sample.getAccessToken(appKey, appSecret);
			rel = teamUserService.synchUserList(accessToken,agentId);
		}catch (Exception e){
	 		log.error(e.getMessage(),e);
		}
		 return Result.ok("同步成功");
	 }


	/**
	 *   添加
	 *
	 * @param teamUser
	 * @return
	 */
	//@AutoLog(value = "团队用户关系-添加")
	@ApiOperation(value="团队用户关系-添加", notes="团队用户关系-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody TeamUser teamUser) {
		teamUserService.save(teamUser);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param teamUser
	 * @return
	 */
	//@AutoLog(value = "团队用户关系-编辑")
	@ApiOperation(value="团队用户关系-编辑", notes="团队用户关系-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody TeamUser teamUser) {
		teamUserService.updateById(teamUser);
		return Result.ok("编辑成功!");
	}

	 /**
	  *  编辑
	  *
	  * @return
	  */
	 @ApiOperation(value="工单类型派单用户-编辑", notes="工单类型派单用户-编辑")
	 @GetMapping(value = "/editPlus")
	 public Result<?> editPlus(HttpServletRequest req) {
		 String teamId=req.getParameter("teamId");
		 String userIds=req.getParameter("userIds");

		 if(!StringUtil.isNullOrEmpty(teamId)&&!StringUtil.isNullOrEmpty(userIds)){
			 teamUserService.editPlus(teamId,userIds);
		 }else{
			 return Result.ok("数据缺失，请联系管理员!");
		 }
		 return Result.ok("操作成功!");
	 }
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "团队用户关系-通过id删除")
	@ApiOperation(value="团队用户关系-通过id删除", notes="团队用户关系-通过id删除")
	@GetMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		teamUserService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	//@AutoLog(value = "团队用户关系-批量删除")
	@ApiOperation(value="团队用户关系-批量删除", notes="团队用户关系-批量删除")
	@GetMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.teamUserService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "团队用户关系-通过id查询")
	@ApiOperation(value="团队用户关系-通过id查询", notes="团队用户关系-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		TeamUser teamUser = teamUserService.getById(id);
		if(teamUser==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(teamUser);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param teamUser
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, TeamUser teamUser) {
        return super.exportXls(request, teamUser, TeamUser.class, "团队用户关系");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, TeamUser.class);
    }

}
