package com.lst.work.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.alibaba.fastjson.JSONObject;
import com.lst.work.entity.*;
import com.lst.work.service.IWorkOrderNumberService;
import com.lst.work.service.IWorkOrderTypeTeamService;
import com.lst.work.service.IWorkOrderTypeUserService;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.api.ISysBaseAPI;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.CommonUtils;
import org.jeecg.common.util.oConvertUtils;
import com.lst.work.service.IWorkOrderTypeService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 工单类型
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Api(tags="工单类型")
@RestController
@RequestMapping("/work/workOrderType")
@Slf4j
public class WorkOrderTypeController extends JeecgController<WorkOrderType, IWorkOrderTypeService> {
	@Autowired
	private IWorkOrderTypeService workOrderTypeService;
	 @Autowired
	 private IWorkOrderTypeUserService workOrderTypeUserService;
	 @Autowired
	 private IWorkOrderNumberService workOrderNumberService;
	 @Autowired
	 private IWorkOrderTypeTeamService workOrderTypeTeamService;
	 @Autowired
	 private ISysBaseAPI sysBaseAPI;
	/**
	 * 分页列表查询
	 *
	 * @param workOrderType
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "工单类型-分页列表查询")
	@ApiOperation(value="工单类型-分页列表查询", notes="工单类型-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(WorkOrderType workOrderType,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WorkOrderType> queryWrapper = QueryGenerator.initQueryWrapper(workOrderType, req.getParameterMap());
		Page<WorkOrderType> page = new Page<WorkOrderType>(pageNo, pageSize);
		IPage<WorkOrderType> pageListTmp = workOrderTypeService.page(page, queryWrapper);
		List<WorkOrderTypeUser> workOrderTypeUsers=workOrderTypeUserService.list();
		IPage<WorkOrderTypeView> pageList=new Page<WorkOrderTypeView>();
		List<WorkOrderTypeView> workOrderTypeViews=new ArrayList<>();
		if(null!=pageListTmp&&null!=pageListTmp.getRecords()&&pageListTmp.getRecords().size()>0){
			workOrderTypeViews = CommonUtils.copyListProperties(pageListTmp.getRecords(), WorkOrderTypeView.class);

			for(WorkOrderTypeView v:workOrderTypeViews){
				List<String> alignUser=new ArrayList<String>();
				if(null!=workOrderTypeUsers&&workOrderTypeUsers.size()>0){
					for(WorkOrderTypeUser user:workOrderTypeUsers){
						if(v.getId().equals(user.getWorkOrderType())){
							alignUser.add(user.getAlignUser());
						}
					}
				}
				v.setAlignUser(alignUser);
			}
		}
		pageList.setCurrent(pageListTmp.getCurrent());
		pageList.setPages(pageListTmp.getPages());
		pageList.setSize(pageListTmp.getSize());
		pageList.setTotal(pageListTmp.getTotal());
		pageList.setRecords(workOrderTypeViews);
		return Result.ok(pageList);
	}

	 /**
	  * 分页列表查询
	  *
	  * @param workOrderType
	  * @param pageNo
	  * @param pageSize
	  * @param req
	  * @return
	  */
	 ////@AutoLog(value = "工单类型-分页列表查询")
	 @ApiOperation(value="工单类型-分页列表查询", notes="工单类型-分页列表查询")
	 @GetMapping(value = "/listAll")
	 public Result<?> queryPageListAll(WorkOrderType workOrderType,
									@RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
									@RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
									HttpServletRequest req) {
		 QueryWrapper<WorkOrderType> queryWrapper = QueryGenerator.initQueryWrapper(workOrderType, req.getParameterMap());
		 Page<WorkOrderType> page = new Page<WorkOrderType>(pageNo, pageSize);
		 IPage<WorkOrderType> pageListTmp = workOrderTypeService.page(page, queryWrapper);
		 QueryWrapper<WorkOrderTypeUser> workOrderTypeUserQueryWrapper=new QueryWrapper<>();
		 //workOrderTypeUserQueryWrapper.eq("tenant_id",workOrderType.getTenantId());
		 List<WorkOrderTypeUser> workOrderTypeUsers=workOrderTypeUserService.list(workOrderTypeUserQueryWrapper);
		 List<JSONObject> workOrderTypeUsersJson=sysBaseAPI.parseDictTextList(workOrderTypeUsers);
		 QueryWrapper<WorkOrderTypeTeam> workOrderTypeTeamQueryWrapper=new QueryWrapper<>();
		 //workOrderTypeTeamQueryWrapper.eq("tenant_id",workOrderType.getTenantId());
		 List<WorkOrderTypeTeam> workOrderTypeTeams=workOrderTypeTeamService.list(workOrderTypeTeamQueryWrapper);
		 List<JSONObject> workOrderTypeTeamsJson=sysBaseAPI.parseDictTextList(workOrderTypeTeams);

		 IPage<WorkOrderTypeView> pageList=new Page<WorkOrderTypeView>();
		 List<WorkOrderTypeView> workOrderTypeViews=new ArrayList<>();
		 if(null!=pageListTmp&&null!=pageListTmp.getRecords()&&pageListTmp.getRecords().size()>0){
			 workOrderTypeViews = CommonUtils.copyListProperties(pageListTmp.getRecords(), WorkOrderTypeView.class);

			 for(WorkOrderTypeView v:workOrderTypeViews){
				 List<String> alignUser=new ArrayList<String>();
				 List<JSONObject> alignUserObj=new ArrayList<JSONObject>();
				 if(null!=workOrderTypeUsersJson&&workOrderTypeUsersJson.size()>0){
					 for(JSONObject user:workOrderTypeUsersJson){
						 if(v.getId().equals(user.get("workOrderType"))){
							 alignUser.add(null!=user.get("alignUser")?(String)user.get("alignUser"):"");
							 alignUserObj.add(user);
						 }
					 }
				 }
				 v.setAlignUser(alignUser);
				 v.setAlignUserObj(alignUserObj);
				 List<String> handleTeam=new ArrayList<String>();
				 List<JSONObject> handleTeamObj=new ArrayList<JSONObject>();
				 if(null!=workOrderTypeTeamsJson&&workOrderTypeTeamsJson.size()>0){
					 for(JSONObject team:workOrderTypeTeamsJson){
						 if(v.getId().equals(team.get("workOrderType"))){
							 handleTeamObj.add(team);
							 handleTeam.add(null!=team.get("team")?(String)team.get("team"):"");
						 }
					 }
				 }
				 v.setHandleTeamObj(handleTeamObj);
				 v.setHandleTeam(handleTeam);

			 }
		 }
		 pageList.setCurrent(pageListTmp.getCurrent());
		 pageList.setPages(pageListTmp.getPages());
		 pageList.setSize(pageListTmp.getSize());
		 pageList.setTotal(pageListTmp.getTotal());
		 pageList.setRecords(workOrderTypeViews);
		 return Result.ok(pageList);
	 }
	
	/**
	 *   添加
	 *
	 * @param workOrderType
	 * @return
	 */
	//@AutoLog(value = "工单类型-添加")
	@ApiOperation(value="工单类型-添加", notes="工单类型-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody WorkOrderTypeView workOrderType) {
		workOrderTypeService.addPlus(workOrderType);
		QueryWrapper<WorkOrderNumber> queryWrapper=new QueryWrapper<WorkOrderNumber>();
		queryWrapper.eq("work_order_type",workOrderType.getId());
		List<WorkOrderNumber> workOrderNumbers=workOrderNumberService.list(queryWrapper);
		if(null==workOrderNumbers||workOrderNumbers.size()==0) {
			WorkOrderNumber workOrderNumber=new WorkOrderNumber();
			workOrderNumber.setWorkOrderType(workOrderType.getId());
			workOrderNumber.setMaxNumber(1);
			workOrderNumberService.save(workOrderNumber);
		}
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @return
	 */
	//@AutoLog(value = "工单类型-编辑")
	@ApiOperation(value="工单类型-编辑", notes="工单类型-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody WorkOrderTypeView workOrderTypeView) {

		if (workOrderTypeService.editPlus(workOrderTypeView))
		{
			QueryWrapper<WorkOrderNumber> queryWrapper=new QueryWrapper<WorkOrderNumber>();
			queryWrapper.eq("work_order_type",workOrderTypeView.getId());
			List<WorkOrderNumber> workOrderNumbers=workOrderNumberService.list(queryWrapper);
			if(null==workOrderNumbers||workOrderNumbers.size()==0) {
				WorkOrderNumber workOrderNumber=new WorkOrderNumber();
				workOrderNumber.setWorkOrderType(workOrderTypeView.getId());
				workOrderNumber.setMaxNumber(1);
				workOrderNumberService.save(workOrderNumber);
			}
			return Result.ok("编辑成功!");
		}else{
			return Result.ok("操作失败!");
		}
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "工单类型-通过id删除")
	@ApiOperation(value="工单类型-通过id删除", notes="工单类型-通过id删除")
	@GetMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		workOrderTypeService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	//@AutoLog(value = "工单类型-批量删除")
	@ApiOperation(value="工单类型-批量删除", notes="工单类型-批量删除")
	@GetMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.workOrderTypeService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "工单类型-通过id查询")
	@ApiOperation(value="工单类型-通过id查询", notes="工单类型-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		WorkOrderType workOrderType = workOrderTypeService.getById(id);
		if(workOrderType==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(workOrderType);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param workOrderType
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WorkOrderType workOrderType) {
        return super.exportXls(request, workOrderType, WorkOrderType.class, "工单类型");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WorkOrderType.class);
    }

}
