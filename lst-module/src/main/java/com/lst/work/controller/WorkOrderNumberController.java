package com.lst.work.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.netty.util.internal.StringUtil;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import com.lst.work.entity.WorkOrderNumber;
import com.lst.work.service.IWorkOrderNumberService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 工单编号
 * @Author: jeecg-boot
 * @Date:   2022-08-13
 * @Version: V1.0
 */
@Api(tags="工单编号")
@RestController
@RequestMapping("/work/workOrderNumber")
@Slf4j
public class WorkOrderNumberController extends JeecgController<WorkOrderNumber, IWorkOrderNumberService> {
	@Autowired
	private IWorkOrderNumberService workOrderNumberService;
	
	/**
	 * 分页列表查询
	 *
	 * @param workOrderNumber
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	//@AutoLog(value = "工单编号-分页列表查询")
	@ApiOperation(value="工单编号-分页列表查询", notes="工单编号-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(WorkOrderNumber workOrderNumber,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WorkOrderNumber> queryWrapper = QueryGenerator.initQueryWrapper(workOrderNumber, req.getParameterMap());
		Page<WorkOrderNumber> page = new Page<WorkOrderNumber>(pageNo, pageSize);
		IPage<WorkOrderNumber> pageList = workOrderNumberService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param workOrderNumber
	 * @return
	 */
	//@AutoLog(value = "工单编号-添加")
	@ApiOperation(value="工单编号-添加", notes="工单编号-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody WorkOrderNumber workOrderNumber) {
		workOrderNumberService.save(workOrderNumber);
		return Result.ok("添加成功！");
	}
	 /**
	  *   获取工单编号
	  *
	  * @return
	  */
	 //@AutoLog(value = "获取工单编号")
	 @ApiOperation(value="获取工单编号", notes="获取工单编号")
	 @GetMapping(value = "/getNumber")
	 public Result<?> add(String workOrderType ) {

		 try{
		 	String rel=workOrderNumberService.getMax(workOrderType);
		 	return Result.ok(rel);
		 }catch (Exception e){
			 return Result.error(e.getMessage());
		 }
//		 return Result.ok("添加成功！");
	 }
	
	/**
	 *  编辑
	 *
	 * @param workOrderNumber
	 * @return
	 */
	//@AutoLog(value = "工单编号-编辑")
	@ApiOperation(value="工单编号-编辑", notes="工单编号-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody WorkOrderNumber workOrderNumber) {
		workOrderNumberService.updateById(workOrderNumber);
		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "工单编号-通过id删除")
	@ApiOperation(value="工单编号-通过id删除", notes="工单编号-通过id删除")
	@GetMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		workOrderNumberService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	//@AutoLog(value = "工单编号-批量删除")
	@ApiOperation(value="工单编号-批量删除", notes="工单编号-批量删除")
	@GetMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.workOrderNumberService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	//@AutoLog(value = "工单编号-通过id查询")
	@ApiOperation(value="工单编号-通过id查询", notes="工单编号-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		WorkOrderNumber workOrderNumber = workOrderNumberService.getById(id);
		if(workOrderNumber==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(workOrderNumber);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param workOrderNumber
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WorkOrderNumber workOrderNumber) {
        return super.exportXls(request, workOrderNumber, WorkOrderNumber.class, "工单编号");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WorkOrderNumber.class);
    }

}
