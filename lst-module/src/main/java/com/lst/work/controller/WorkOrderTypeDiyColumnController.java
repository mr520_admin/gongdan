package com.lst.work.controller;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import io.netty.util.internal.StringUtil;
import org.jeecg.common.api.vo.Result;
import org.jeecg.common.system.query.QueryGenerator;
import org.jeecg.common.util.oConvertUtils;
import com.lst.work.entity.WorkOrderTypeDiyColumn;
import com.lst.work.service.IWorkOrderTypeDiyColumnService;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.extern.slf4j.Slf4j;

import org.jeecgframework.poi.excel.ExcelImportUtil;
import org.jeecgframework.poi.excel.def.NormalExcelConstants;
import org.jeecgframework.poi.excel.entity.ExportParams;
import org.jeecgframework.poi.excel.entity.ImportParams;
import org.jeecgframework.poi.excel.view.JeecgEntityExcelView;
import org.jeecg.common.system.base.controller.JeecgController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.jeecg.common.aspect.annotation.AutoLog;

 /**
 * @Description: 工单自定义字段
 * @Author: jeecg-boot
 * @Date:   2022-08-26
 * @Version: V1.0
 */
@Api(tags="工单自定义字段")
@RestController
@RequestMapping("/work/workOrderTypeDiyColumn")
@Slf4j
public class WorkOrderTypeDiyColumnController extends JeecgController<WorkOrderTypeDiyColumn, IWorkOrderTypeDiyColumnService> {
	@Autowired
	private IWorkOrderTypeDiyColumnService workOrderTypeDiyColumnService;
	
	/**
	 * 分页列表查询
	 *
	 * @param workOrderTypeDiyColumn
	 * @param pageNo
	 * @param pageSize
	 * @param req
	 * @return
	 */
	@AutoLog(value = "工单自定义字段-分页列表查询")
	@ApiOperation(value="工单自定义字段-分页列表查询", notes="工单自定义字段-分页列表查询")
	@GetMapping(value = "/list")
	public Result<?> queryPageList(WorkOrderTypeDiyColumn workOrderTypeDiyColumn,
								   @RequestParam(name="pageNo", defaultValue="1") Integer pageNo,
								   @RequestParam(name="pageSize", defaultValue="10") Integer pageSize,
								   HttpServletRequest req) {
		QueryWrapper<WorkOrderTypeDiyColumn> queryWrapper = QueryGenerator.initQueryWrapper(workOrderTypeDiyColumn, req.getParameterMap());
		Page<WorkOrderTypeDiyColumn> page = new Page<WorkOrderTypeDiyColumn>(pageNo, pageSize);
		IPage<WorkOrderTypeDiyColumn> pageList = workOrderTypeDiyColumnService.page(page, queryWrapper);
		return Result.ok(pageList);
	}
	
	/**
	 *   添加
	 *
	 * @param workOrderTypeDiyColumn
	 * @return
	 */
	@AutoLog(value = "工单自定义字段-添加")
	@ApiOperation(value="工单自定义字段-添加", notes="工单自定义字段-添加")
	@PostMapping(value = "/add")
	public Result<?> add(@RequestBody WorkOrderTypeDiyColumn workOrderTypeDiyColumn) {
		if(!StringUtil.isNullOrEmpty(workOrderTypeDiyColumn.getAutoInputFromColumn())&&!StringUtil.isNullOrEmpty(workOrderTypeDiyColumn.getWorkOrderType())){
			QueryWrapper<WorkOrderTypeDiyColumn> queryWrapper=new QueryWrapper<>();
			queryWrapper.eq("auto_input_from_column",workOrderTypeDiyColumn.getAutoInputFromColumn());
			queryWrapper.eq("work_order_type",workOrderTypeDiyColumn.getWorkOrderType());
			List<WorkOrderTypeDiyColumn> columns=workOrderTypeDiyColumnService.list(queryWrapper);
			if(null!=columns&&columns.size()>0){
				WorkOrderTypeDiyColumn tmp=columns.get(0);
				workOrderTypeDiyColumn.setComponentType(tmp.getComponentType());
				workOrderTypeDiyColumn.setMultiple(tmp.getMultiple());
				workOrderTypeDiyColumn.setDictJson(tmp.getDictJson());
			}else{
				return Result.ok("自动带入字段不存在");
			}
		}
		workOrderTypeDiyColumnService.save(workOrderTypeDiyColumn);
		return Result.ok("添加成功！");
	}
	
	/**
	 *  编辑
	 *
	 * @param workOrderTypeDiyColumn
	 * @return
	 */
	@AutoLog(value = "工单自定义字段-编辑")
	@ApiOperation(value="工单自定义字段-编辑", notes="工单自定义字段-编辑")
	@PutMapping(value = "/edit")
	public Result<?> edit(@RequestBody WorkOrderTypeDiyColumn workOrderTypeDiyColumn) {
		workOrderTypeDiyColumnService.updateById(workOrderTypeDiyColumn);
		if(!StringUtil.isNullOrEmpty(workOrderTypeDiyColumn.getColumnCode())&&!StringUtil.isNullOrEmpty(workOrderTypeDiyColumn.getWorkOrderType())){
			QueryWrapper<WorkOrderTypeDiyColumn> queryWrapper=new QueryWrapper<>();
			queryWrapper.eq("auto_input_from_column",workOrderTypeDiyColumn.getColumnCode());
			queryWrapper.eq("work_order_type",workOrderTypeDiyColumn.getWorkOrderType());
			List<WorkOrderTypeDiyColumn> columns=workOrderTypeDiyColumnService.list(queryWrapper);
			if(null!=columns&&columns.size()>0){
				for(WorkOrderTypeDiyColumn tmp:columns){
					tmp.setComponentType(workOrderTypeDiyColumn.getComponentType());
					tmp.setMultiple(workOrderTypeDiyColumn.getMultiple());
					tmp.setDictJson(workOrderTypeDiyColumn.getDictJson());
					workOrderTypeDiyColumnService.updateById(tmp);
				}
			}
		}

		return Result.ok("编辑成功!");
	}
	
	/**
	 *   通过id删除
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "工单自定义字段-通过id删除")
	@ApiOperation(value="工单自定义字段-通过id删除", notes="工单自定义字段-通过id删除")
	@GetMapping(value = "/delete")
	public Result<?> delete(@RequestParam(name="id",required=true) String id) {
		workOrderTypeDiyColumnService.removeById(id);
		return Result.ok("删除成功!");
	}
	
	/**
	 *  批量删除
	 *
	 * @param ids
	 * @return
	 */
	@AutoLog(value = "工单自定义字段-批量删除")
	@ApiOperation(value="工单自定义字段-批量删除", notes="工单自定义字段-批量删除")
	@GetMapping(value = "/deleteBatch")
	public Result<?> deleteBatch(@RequestParam(name="ids",required=true) String ids) {
		this.workOrderTypeDiyColumnService.removeByIds(Arrays.asList(ids.split(",")));
		return Result.ok("批量删除成功!");
	}
	
	/**
	 * 通过id查询
	 *
	 * @param id
	 * @return
	 */
	@AutoLog(value = "工单自定义字段-通过id查询")
	@ApiOperation(value="工单自定义字段-通过id查询", notes="工单自定义字段-通过id查询")
	@GetMapping(value = "/queryById")
	public Result<?> queryById(@RequestParam(name="id",required=true) String id) {
		WorkOrderTypeDiyColumn workOrderTypeDiyColumn = workOrderTypeDiyColumnService.getById(id);
		if(workOrderTypeDiyColumn==null) {
			return Result.error("未找到对应数据");
		}
		return Result.ok(workOrderTypeDiyColumn);
	}

    /**
    * 导出excel
    *
    * @param request
    * @param workOrderTypeDiyColumn
    */
    @RequestMapping(value = "/exportXls")
    public ModelAndView exportXls(HttpServletRequest request, WorkOrderTypeDiyColumn workOrderTypeDiyColumn) {
        return super.exportXls(request, workOrderTypeDiyColumn, WorkOrderTypeDiyColumn.class, "工单自定义字段");
    }

    /**
      * 通过excel导入数据
    *
    * @param request
    * @param response
    * @return
    */
    @RequestMapping(value = "/importExcel", method = RequestMethod.POST)
    public Result<?> importExcel(HttpServletRequest request, HttpServletResponse response) {
        return super.importExcel(request, response, WorkOrderTypeDiyColumn.class);
    }

}
