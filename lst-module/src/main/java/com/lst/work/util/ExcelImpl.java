package com.lst.work.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

import javax.servlet.ServletOutputStream;

@Slf4j
public class ExcelImpl {

  public void export(HSSFWorkbook workbook, ServletOutputStream out) throws Exception{
    try{

      // 将文件输出到客户端浏览器

        workbook.write(out);
        out.flush();
        out.close();

    }catch(Exception e){

      log.error(e.getMessage(),e);

      throw new Exception("导出信息失败！");

    }
  }
}