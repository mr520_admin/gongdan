package com.lst.work.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 工单自定义字段
 * @Author: jeecg-boot
 * @Date:   2022-08-26
 * @Version: V1.0
 */
@Data
@TableName("work_order_type_diy_column")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="work_order_type_diy_column对象", description="工单自定义字段")
public class WorkOrderTypeDiyColumn implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd")
    @DateTimeFormat(pattern="yyyy-MM-dd")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**排序*/
	@Excel(name = "排序", width = 15)
    @ApiModelProperty(value = "排序")
    private java.lang.String orderNum;
	/**类别ID*/
	@Excel(name = "类别ID", width = 15, dictTable = "work_order_type", dicText = "name", dicCode = "id")
	@Dict(dictTable = "work_order_type", dicText = "name", dicCode = "id")
    @ApiModelProperty(value = "类别ID")
    private java.lang.String workOrderType;
    /**域*/
    @Excel(name = "域", width = 15, dicCode = "scope")
    @Dict(dicCode = "scope")
    @ApiModelProperty(value = "域")
    private java.lang.String scope;
	/**字段编号*/
	@Excel(name = "字段编号", width = 15)
    @ApiModelProperty(value = "字段编号")
    private java.lang.String columnCode;
	/**字段名称*/
	@Excel(name = "字段名称", width = 15)
    @ApiModelProperty(value = "字段名称")
    private java.lang.String columnName;
	/**是否显示*/
	@Excel(name = "是否显示", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否显示")
    private java.lang.String isShow;
	/**组件类型*/
	@Excel(name = "组件类型", width = 15, dicCode = "component_type")
	@Dict(dicCode = "component_type")
    @ApiModelProperty(value = "组件类型")
    private java.lang.String componentType;
	/**是否必填*/
	@Excel(name = "是否必填", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否必填")
    private java.lang.String required;
	/**是否重复工单判断依据字段*/
	@Excel(name = "是否重复工单判断依据字段", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否重复工单判断依据字段")
    private java.lang.String duplicatedMark;
    /**是否自带字段*/
    @Excel(name = "是否自带字段", width = 15, dicCode = "yn")
    @Dict(dicCode = "yn")
    private String mainColumn;
    /**是否支持多选*/
    @Excel(name = "是否支持多选", width = 15, dicCode = "yn")
    @Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否支持多选")
    private java.lang.String multiple;
	/**字典Json*/
	@Excel(name = "字典Json", width = 15)
    @ApiModelProperty(value = "字典Json")
    private java.lang.String dictJson;

	/**初始值自动带入的来源字段*/
    @Excel(name = "初始值自动带入的来源字段", width = 15)
    @ApiModelProperty(value = "初始值自动带入的来源字段")
    private java.lang.String autoInputFromColumn;

    /**列表是否展示*/
    @Excel(name = "列表是否展示", width = 15, dicCode = "yn")
    @Dict(dicCode = "yn")
    @ApiModelProperty(value = "列表是否展示")
    private java.lang.String listShow;

    /**消息是否展示*/
    @Excel(name = "消息是否展示", width = 15, dicCode = "yn")
    @Dict(dicCode = "yn")
    @ApiModelProperty(value = "消息是否展示")
    private java.lang.String msgShow;

    /**筛选是否展示*/
    @Excel(name = "筛选是否展示", width = 15, dicCode = "yn")
    @Dict(dicCode = "yn")
    @ApiModelProperty(value = "筛选是否展示")
    private java.lang.String filterShow;

    /**校验类型*/
    @Excel(name = "校验类型", width = 15, dicCode = "validType")
    @Dict(dicCode = "validType")
    @ApiModelProperty(value = "校验类型")
    private java.lang.String validType;

}
