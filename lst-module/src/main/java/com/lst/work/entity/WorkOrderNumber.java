package com.lst.work.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 工单编号
 * @Author: jeecg-boot
 * @Date:   2022-08-13
 * @Version: V1.0
 */
@Data
@TableName("work_order_number")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="work_order_number对象", description="工单编号")
public class WorkOrderNumber implements Serializable {
    private static final long serialVersionUID = 1L;
    /**主键*/
    @TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**工单类型*/
	@Excel(name = "工单类型", width = 15)
    @ApiModelProperty(value = "工单类型")
    private java.lang.String workOrderType;
	/**maxNumber*/
	@Excel(name = "maxNumber", width = 15)
    @ApiModelProperty(value = "maxNumber")
    private java.lang.Integer maxNumber;
}
