package com.lst.work.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 钉钉消息
 * @Author: jeecg-boot
 * @Date:   2022-08-22
 * @Version: V1.0
 */
@Data
@TableName("ding_msg")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="ding_msg对象", description="钉钉消息")
public class DingMsg implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**工单id*/
	@Excel(name = "工单id", width = 15)
    @ApiModelProperty(value = "工单id")
    private java.lang.String workOrder;
	/**工单编号*/
	@Excel(name = "工单编号", width = 15)
    @ApiModelProperty(value = "工单编号")
    private java.lang.String workNumber;
	/**发送目标用户*/
	@Excel(name = "发送目标用户", width = 15)
    @ApiModelProperty(value = "发送目标用户")
    private java.lang.String toUser;
	/**access_token*/
	@Excel(name = "access_token", width = 15)
    @ApiModelProperty(value = "access_token")
    private java.lang.String accessToken;
	/**agent_id*/
	@Excel(name = "agent_id", width = 15)
    @ApiModelProperty(value = "agent_id")
    private java.lang.String agentId;
	/**发送报文*/
	@Excel(name = "发送报文", width = 15)
    @ApiModelProperty(value = "发送报文")
    private java.lang.String sendMsg;
    /**工单环节*/
    @Excel(name = "工单环节", width = 15)
    @ApiModelProperty(value = "工单环节")
    private java.lang.String workOrderStatus;
	/**是否请求成功*/
	@Excel(name = "是否请求成功", width = 15)
    @ApiModelProperty(value = "是否请求成功")
    private java.lang.String sendSuccess;
	/**接收报文*/
	@Excel(name = "接收报文", width = 15)
    @ApiModelProperty(value = "接收报文")
    private java.lang.String receiveMsg;
	/**钉钉任务编号*/
	@Excel(name = "钉钉任务编号", width = 15)
    @ApiModelProperty(value = "钉钉任务编号")
    private java.lang.String taskId;
	/**查询是否送达*/
	@Excel(name = "查询是否送达", width = 15)
    @ApiModelProperty(value = "查询是否送达")
    private java.lang.String checkMessage;
    /**消息类型*/
    @Excel(name = "消息类型", width = 15, dicCode = "ding_msg_type")
    @Dict(dicCode = "ding_msg_type")
    @ApiModelProperty(value = "消息类型")
    private java.lang.String dingMsgType;
    /**告警次数*/
    @Excel(name = "告警次数", width = 15)
    @ApiModelProperty(value = "告警次数")
    private Integer warnTimes;
}
