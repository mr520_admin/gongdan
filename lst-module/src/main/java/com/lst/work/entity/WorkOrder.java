package com.lst.work.entity;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.format.annotation.DateTimeFormat;
import org.jeecgframework.poi.excel.annotation.Excel;
import org.jeecg.common.aspect.annotation.Dict;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * @Description: 工单
 * @Author: jeecg-boot
 * @Date:   2022-08-07
 * @Version: V1.0
 */
@Data
@TableName("work_order")
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@ApiModel(value="work_order对象", description="工单")
public class WorkOrder implements Serializable {
    private static final long serialVersionUID = 1L;

	/**主键*/
	@TableId(type = IdType.ID_WORKER_STR)
    @ApiModelProperty(value = "主键")
    private java.lang.String id;
	/**创建人*/
    @ApiModelProperty(value = "创建人")
    private java.lang.String createBy;
	/**创建日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "创建日期")
    private java.util.Date createTime;
	/**更新人*/
    @ApiModelProperty(value = "更新人")
    private java.lang.String updateBy;
	/**更新日期*/
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "更新日期")
    private java.util.Date updateTime;
	/**所属部门*/
    @ApiModelProperty(value = "所属部门")
    private java.lang.String sysOrgCode;
	/**服务项目*/
	@Excel(name = "服务项目", width = 15, dictTable = "work_order_type", dicText = "name", dicCode = "id")
	@Dict(dictTable = "work_order_type", dicText = "name", dicCode = "id")
    @ApiModelProperty(value = "服务项目")
    private java.lang.String type;
	/**工单编号*/
	@Excel(name = "工单编号", width = 15)
    @ApiModelProperty(value = "工单编号")
    private java.lang.String number;
	/**问题现象*/
	@Excel(name = "问题现象", width = 15, dictTable = "problem_type", dicText = "name", dicCode = "id")
	@Dict(dictTable = "problem_type", dicText = "name", dicCode = "id")
    @ApiModelProperty(value = "问题现象")
    private java.lang.String problemTag;
	/**故障号码*/
	@Excel(name = "故障号码", width = 15)
    @ApiModelProperty(value = "故障号码")
    private java.lang.String userNo;
	/**联系人*/
	@Excel(name = "联系人", width = 15)
	@ApiModelProperty(value = "联系人")
	private java.lang.String contactUser;
	/**联系电话*/
	@Excel(name = "联系电话", width = 15)
	@ApiModelProperty(value = "联系电话")
	private java.lang.String contactPhone;
	/**报障人员*/
	@Excel(name = "报障人员", width = 15, dictTable = "sys_user", dicText = "realname", dicCode = "id")
	@Dict(dictTable = "sys_user", dicText = "realname", dicCode = "id")
    @ApiModelProperty(value = "报障人员")
    private java.lang.String fromUser;
	/**工单状态*/
	@Excel(name = "工单状态", width = 15, dicCode = "work_order_status")
	@Dict(dicCode = "work_order_status")
    @ApiModelProperty(value = "工单状态")
    private java.lang.String status;
	/**派单人员*/
	@Excel(name = "派单人员", width = 15, dictTable = "sys_user", dicText = "realname", dicCode = "id")
	@Dict(dictTable = "sys_user", dicText = "realname", dicCode = "id")
    @ApiModelProperty(value = "派单人员")
    private java.lang.String alignUser;
	/**快速处理标记*/
	@Excel(name = "快速处理标记", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
	@ApiModelProperty(value = "快速处理标记")
	private java.lang.String quickHandleMark;
	/**快速处理说明*/
	@Excel(name = "快速处理说明", width = 15)
	@ApiModelProperty(value = "快速处理说明")
	private java.lang.String quickHandleRemark;
	/**到达派单时间*/
	@Excel(name = "到达派单时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "到达派单时间")
	private java.util.Date toAlignTime;
	/**首次派单时间*/
	@Excel(name = "首次派单时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "首次派单时间")
	private java.util.Date firstAlignTime;
	/**派单时间*/
	@Excel(name = "派单时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "派单时间")
    private java.util.Date alignTime;
	/**接单时间*/
	@Excel(name = "接单时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "接单时间")
	private java.util.Date getTime;
	/**预约处理时间*/
	@Excel(name = "预约处理时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty(value = "预约处理时间")
	private java.util.Date planServiceTime;
	/**客户地址*/
	@Excel(name = "客户地址", width = 15)
    @ApiModelProperty(value = "客户地址")
    private java.lang.String address;
	/**工程师*/
	@Excel(name = "工程师", width = 15, dictTable = "sys_user", dicText = "realname", dicCode = "id")
	@Dict(dictTable = "sys_user", dicText = "realname", dicCode = "id")
    @ApiModelProperty(value = "工程师")
    private java.lang.String toUser;
	/**工单备注*/
	@Excel(name = "工单备注", width = 15)
    @ApiModelProperty(value = "工单备注")
    private java.lang.String remark;
	/**是否解决*/
	@Excel(name = "是否解决", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
    @ApiModelProperty(value = "是否解决")
    private java.lang.String isSolve;
	/**系统自动定位*/
	@ApiModelProperty(value = "系统自动定位")
	private java.lang.String location;
	/**是否解决*/
	@Excel(name = "解决备注", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
	@ApiModelProperty(value = "解决备注")
	private java.lang.String solveRemark;

	/**是否解决*/
	@Excel(name = "解决确认备注", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
	@ApiModelProperty(value = "解决确认备注")
	private java.lang.String isSolveVerifyRemark;
	/**是否解决*/
	@Excel(name = "是否解决确认", width = 15, dicCode = "yn")
	@Dict(dicCode = "yn")
	@ApiModelProperty(value = "是否解决确认")
	private java.lang.String isSolveVerify;
	/**解决时间*/
	@Excel(name = "解决时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "解决时间")
    private java.util.Date solveTime;
	/**评分*/
	@Excel(name = "评分", width = 15)
    @ApiModelProperty(value = "评分")
    private java.lang.String score;
	/**文件*/
	@Excel(name = "文件", width = 15)
	@ApiModelProperty(value = "文件")
	private java.lang.String file;
	/**评分人员*/
	@Excel(name = "评分人员", width = 15, dictTable = "sys_user", dicText = "realname", dicCode = "id")
	@Dict(dictTable = "sys_user", dicText = "realname", dicCode = "id")
    private java.lang.String scoreUser;
	/**派单时间*/
	@Excel(name = "派单时间", width = 20, format = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(timezone = "GMT+8",pattern = "yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value = "派单时间")
    private java.util.Date scoreTime;
	/**30天重复报障*/
	@Excel(name = "30天重复报障", width = 15)
    @ApiModelProperty(value = "30天重复报障")
    private Integer duplicated30;
	/**60天重复报障*/
	@Excel(name = "60天重复报障", width = 15)
    @ApiModelProperty(value = "60天重复报障")
    private Integer duplicated60;
	/**90天重复报障*/
	@Excel(name = "90天重复报障", width = 15)
    @ApiModelProperty(value = "90天重复报障")
    private Integer duplicated90;

	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段1", width = 15)
	@ApiModelProperty(value = "报障扩展字段1")
	private java.lang.String baozhangExt1;
	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段1", width = 15)
	@ApiModelProperty(value = "报障扩展字段1")
	private java.lang.String baozhangExt2;
	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段1", width = 15)
	@ApiModelProperty(value = "报障扩展字段1")
	private java.lang.String baozhangExt3;
	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段5", width = 15)
	@ApiModelProperty(value = "报障扩展字段5")
	private java.lang.String baozhangExt4;
	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段5", width = 15)
	@ApiModelProperty(value = "报障扩展字段5")
	private java.lang.String baozhangExt5;
	/**报障扩展字段6*/
	@Excel(name = "报障扩展字段6", width = 15)
	@ApiModelProperty(value = "报障扩展字段6")
	private java.lang.String baozhangExt6;
	/**报障扩展字段7*/
	@Excel(name = "报障扩展字段7", width = 15)
	@ApiModelProperty(value = "报障扩展字段7")
	private java.lang.String baozhangExt7;
	/**报障扩展字段8*/
	@Excel(name = "报障扩展字段8", width = 15)
	@ApiModelProperty(value = "报障扩展字段8")
	private java.lang.String baozhangExt8;
	/**报障扩展字段9*/
	@Excel(name = "报障扩展字段9", width = 15)
	@ApiModelProperty(value = "报障扩展字段9")
	private java.lang.String baozhangExt9;
	/**报障扩展字段10*/
	@Excel(name = "报障扩展字段10", width = 15)
	@ApiModelProperty(value = "报障扩展字段10")
	private java.lang.String baozhangExt10;


	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段1", width = 15)
	@ApiModelProperty(value = "报障扩展字段1")
	private java.lang.String chuliExt1;
	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段1", width = 15)
	@ApiModelProperty(value = "报障扩展字段1")
	private java.lang.String chuliExt2;
	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段1", width = 15)
	@ApiModelProperty(value = "报障扩展字段1")
	private java.lang.String chuliExt3;
	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段5", width = 15)
	@ApiModelProperty(value = "报障扩展字段5")
	private java.lang.String chuliExt4;
	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段5", width = 15)
	@ApiModelProperty(value = "报障扩展字段5")
	private java.lang.String chuliExt5;
	/**报障扩展字段6*/
	@Excel(name = "报障扩展字段6", width = 15)
	@ApiModelProperty(value = "报障扩展字段6")
	private java.lang.String chuliExt6;
	/**报障扩展字段7*/
	@Excel(name = "报障扩展字段7", width = 15)
	@ApiModelProperty(value = "报障扩展字段7")
	private java.lang.String chuliExt7;
	/**报障扩展字段8*/
	@Excel(name = "报障扩展字段8", width = 15)
	@ApiModelProperty(value = "报障扩展字段8")
	private java.lang.String chuliExt8;
	/**报障扩展字段9*/
	@Excel(name = "报障扩展字段9", width = 15)
	@ApiModelProperty(value = "报障扩展字段9")
	private java.lang.String chuliExt9;
	/**报障扩展字段10*/
	@Excel(name = "报障扩展字段10", width = 15)
	@ApiModelProperty(value = "报障扩展字段10")
	private java.lang.String chuliExt10;





	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段1", width = 15)
	@ApiModelProperty(value = "报障扩展字段1")
	private java.lang.String pingjiaExt1;
	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段1", width = 15)
	@ApiModelProperty(value = "报障扩展字段1")
	private java.lang.String pingjiaExt2;
	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段1", width = 15)
	@ApiModelProperty(value = "报障扩展字段1")
	private java.lang.String pingjiaExt3;
	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段5", width = 15)
	@ApiModelProperty(value = "报障扩展字段5")
	private java.lang.String pingjiaExt4;
	/**报障扩展字段1*/
	@Excel(name = "报障扩展字段5", width = 15)
	@ApiModelProperty(value = "报障扩展字段5")
	private java.lang.String pingjiaExt5;
	/**报障扩展字段6*/
	@Excel(name = "报障扩展字段6", width = 15)
	@ApiModelProperty(value = "报障扩展字段6")
	private java.lang.String pingjiaExt6;
	/**报障扩展字段7*/
	@Excel(name = "报障扩展字段7", width = 15)
	@ApiModelProperty(value = "报障扩展字段7")
	private java.lang.String pingjiaExt7;
	/**报障扩展字段8*/
	@Excel(name = "报障扩展字段8", width = 15)
	@ApiModelProperty(value = "报障扩展字段8")
	private java.lang.String pingjiaExt8;
	/**报障扩展字段9*/
	@Excel(name = "报障扩展字段9", width = 15)
	@ApiModelProperty(value = "报障扩展字段9")
	private java.lang.String pingjiaExt9;
	/**报障扩展字段10*/
	@Excel(name = "报障扩展字段10", width = 15)
	@ApiModelProperty(value = "报障扩展字段10")
	private java.lang.String pingjiaExt10;


	/**现场照片*/
	@Excel(name = "现场照片", width = 15)
	@ApiModelProperty(value = "现场照片")
	private java.lang.String solvePic;
	/**用户是否满意*/
	@Excel(name = "用户是否满意", width = 15)
	@ApiModelProperty(value = "用户是否满意")
	private java.lang.String scoreUserSatisfied;
	/**用户意见*/
	@Excel(name = "用户意见", width = 15)
	@ApiModelProperty(value = "用户意见")
	private java.lang.String scoreUserOpinion;
	/**省*/
	@Excel(name = "省", width = 15)
	@ApiModelProperty(value = "省")
	private java.lang.String province;
	/**省份名称*/
	@Excel(name = "省份名称", width = 15)
	@ApiModelProperty(value = "省份名称")
	private java.lang.String provinceName;
	/**地市*/
	@Excel(name = "地市", width = 15)
	@ApiModelProperty(value = "地市")
	private java.lang.String city;
	/**地市名称*/
	@Excel(name = "地市名称", width = 15)
	@ApiModelProperty(value = "地市名称")
	private java.lang.String cityName;
	/**区县*/
	@Excel(name = "区县", width = 15)
	@ApiModelProperty(value = "区县")
	private java.lang.String county;
	/**区县名称*/
	@Excel(name = "区县名称", width = 15)
	@ApiModelProperty(value = "区县名称")
	private java.lang.String countyName;
	/**镇*/
	@Excel(name = "镇", width = 15, dictTable = "town", dicText = "name", dicCode = "id")
	@Dict(dictTable = "town", dicText = "name", dicCode = "id")
	@ApiModelProperty(value = "镇")
	private java.lang.String town;
	/**镇名称*/
	@Excel(name = "镇名称", width = 15)
	@ApiModelProperty(value = "镇名称")
	private java.lang.String townName;
	/**报障类型*/
	@Excel(name = "报障类型", width = 15, dicCode = "baozhangType")
	@Dict(dicCode = "baozhangType")
	@ApiModelProperty(value = "报障类型")
	private java.lang.String baozhangType;

}
