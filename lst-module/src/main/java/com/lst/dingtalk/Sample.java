// This file is auto-generated, don't edit it. Thanks.
package com.lst.dingtalk;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyun.tea.*;
import com.aliyun.teautil.*;
import com.aliyun.dingtalkoauth2_1_0.*;
import com.aliyun.dingtalkoauth2_1_0.models.*;
import com.aliyun.teaopenapi.*;
import com.aliyun.teaopenapi.models.*;
import com.dingtalk.api.DefaultDingTalkClient;
import com.dingtalk.api.DingTalkClient;
import com.dingtalk.api.request.OapiMessageCorpconversationAsyncsendV2Request;
import com.dingtalk.api.request.OapiV2DepartmentGetRequest;
import com.dingtalk.api.request.OapiV2DepartmentListsubidRequest;
import com.dingtalk.api.request.OapiV2UserListRequest;
import com.dingtalk.api.response.OapiMessageCorpconversationAsyncsendV2Response;
import com.dingtalk.api.response.OapiV2DepartmentGetResponse;
import com.dingtalk.api.response.OapiV2DepartmentListsubidResponse;
import com.dingtalk.api.response.OapiV2UserListResponse;
import com.google.gson.JsonObject;
import com.taobao.api.ApiException;
import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;
import org.apache.shiro.crypto.hash.Hash;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class Sample {

//    public  static  final Long agentId=1783738475L;
//    public static final String appKey="dingtuz8vfjqvnvddhaj";
//    public static final String appSecret="bVMgsaID9XcC81avmb-QP6QEveOLV_04llL6JnyUfenrVK6-_jMe50-Vw1akSQke";
    /**
     * 使用 Token 初始化账号Client
     * @return Client
     * @throws Exception
     */
    public static com.aliyun.dingtalkoauth2_1_0.Client createClient() throws Exception {
        Config config = new Config();
        config.protocol = "https";
        config.regionId = "central";
        return new com.aliyun.dingtalkoauth2_1_0.Client(config);
    }

    public static String getAccessToken(String appKey,String appSecret) throws Exception {
        java.util.List<String> args = java.util.Arrays.asList();
        com.aliyun.dingtalkoauth2_1_0.Client client = Sample.createClient();
        GetAccessTokenRequest getAccessTokenRequest = new GetAccessTokenRequest()
                .setAppKey(appKey)
                .setAppSecret(appSecret);
        try {
            GetAccessTokenResponse aa=client.getAccessToken(getAccessTokenRequest);
//            System.out.println(aa.body.accessToken);
            return aa.body.accessToken;
        } catch (TeaException err) {
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
            }

        } catch (Exception _err) {
            TeaException err = new TeaException(_err.getMessage(), _err);
            if (!com.aliyun.teautil.Common.empty(err.code) && !com.aliyun.teautil.Common.empty(err.message)) {
                // err 中含有 code 和 message 属性，可帮助开发定位问题
            }

        }
        return "";
    }

    public static void main(String[] args) throws Exception {
        String accessToken=Sample.getAccessToken("dingvlivnohmzudn3eve","y2V_nvGgsYS5N-QyNWksQF0SOWnC2NIPEFhz6MrQkQeWL3CU5DjPfBPirIDeDH4B");
        System.out.println(accessToken);
//        List<Integer> depts=new ArrayList<Integer>();
//        depts.add(1);
//        Sample.getSonDepart(depts,1,accessToken,true);
//        List<Object> rel=new ArrayList<Object>();
//        for(Integer d:depts){
//            Object obj=Sample.getDeptDetail(d,accessToken);
//        }
//        for(Integer d:depts){
//            List<JSONObject> jsonObjects=Sample.getUserByDept(d,accessToken);
//            System.out.println("--------"+d);
//            for(JSONObject jsonObject: jsonObjects){
//
//                System.out.println(JSONObject.toJSONString(jsonObjects));
//                System.out.println("姓名:"+(null!=jsonObject.get("name")?(String)jsonObject.get("name"):""));
//                System.out.println("用户Id:"+(null!=jsonObject.get("userid")?(String)jsonObject.get("userid"):""));
//                System.out.println("头像:"+(null!=jsonObject.get("avatar")?(String)jsonObject.get("avatar"):""));
//                System.out.println("手机号:"+(null!=jsonObject.get("mobile")?(String)jsonObject.get("mobile"):""));
//            }
//
//        }
//        String useridsList="061861562132840542";
//        String useridsList="24111809271224114";
//
//        String url="http://www.sina.com";
//        String title="您有新的[待办]工单";
//        SimpleDateFormat sdf=new SimpleDateFormat("HH:mm:ss");
//        String content="亲,来工单了,送达时间"+sdf.format(new Date())+",点击即可查看办理！";
//        HashMap<String,String> forms=new LinkedHashMap<String,String>();
//        forms.put("工单编号:","Y000000054");
//        forms.put("项目类型:","宽带业务");
//        forms.put("问题类型:","没信号");
//        forms.put("问题账号:","158542365478");
//        forms.put("问题地址:","渝北区迎宾社区黄桷垭街皇家一号公馆懂15号");
//        Sample.sendOaMsg(accessToken,null,useridsList,url,title,content,forms);

    }
    public static List<Integer> getSonDepart(List<Integer> depts,Integer curDept,String accessToken,boolean digui){
        try {

            DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/department/listsubid");
            OapiV2DepartmentListsubidRequest req = new OapiV2DepartmentListsubidRequest();
            req.setDeptId(Long.valueOf(curDept));
//            String accessToken="";
//            accessToken=Sample.getAccessToken();
//            if(StringUtil.isNullOrEmpty(accessToken)){
//                accessToken=Sample.getAccessToken(appK);
//            }
            OapiV2DepartmentListsubidResponse rsp = client.execute(req,accessToken );
            System.out.println(rsp.getBody());
            JSONObject jsonObject=JSONObject.parseObject(rsp.getBody());
            Integer errcode=null!=jsonObject.get("errcode")?(Integer)jsonObject.get("errcode"):666;
            String errmsg=null!=jsonObject.get("errmsg")?(String)jsonObject.get("errmsg"):"";
            if(0==errcode&&"ok".equals(errmsg)) {
                JSONObject objJson = null != jsonObject.get("result") ? (JSONObject)jsonObject.get("result") : null;
                JSONArray deptIds = null != objJson ? (JSONArray) objJson.get("dept_id_list") : null;
                if (null != deptIds && deptIds.size() > 0) {
                    for (int i=0;i<deptIds.size();i++) {
                        Integer d=(Integer)deptIds.get(i);
                        depts.add(d);
                        if(digui) {
                            depts = getSonDepart(depts, d, accessToken,digui);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return depts;
    }
    public static Object getDeptDetail(Integer dept,String accessToken) {
        Object rel=null;
        try {
            DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/department/get");
            OapiV2DepartmentGetRequest req = new OapiV2DepartmentGetRequest();
            req.setDeptId(Long.valueOf(dept));
//            if(StringUtil.isNullOrEmpty(accessToken)){
//                accessToken=Sample.getAccessToken();
//            }
            OapiV2DepartmentGetResponse rsp = client.execute(req, accessToken);
//            System.out.println(rsp.getBody());
            log.info(rsp.getBody());
            rel=rsp.getBody();
        } catch (Exception e) {
            log.error(e.getMessage(),e);
            e.printStackTrace();
        }
        return rel;
    }

    public static List<JSONObject> getUserByDept(Integer dept,String accessToken){
        List<JSONObject> users=new ArrayList<JSONObject>();
        try {
            DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/v2/user/list");
            OapiV2UserListRequest req = new OapiV2UserListRequest();
//            if(StringUtil.isNullOrEmpty(accessToken)){
//                accessToken=Sample.getAccessToken();
//            }
            req.setDeptId(Long.valueOf(dept));
            boolean mark=true;
            long cursor=0l;
            long size=10l;
            while (mark) {
                req.setCursor(cursor);
                req.setSize(size);
                OapiV2UserListResponse rsp = client.execute(req, accessToken);
//                System.out.println(rsp.getBody());
                log.info(rsp.getBody());
                JSONObject jsonObject = JSONObject.parseObject(rsp.getBody());
                Integer errcode = null != jsonObject.get("errcode") ? (Integer) jsonObject.get("errcode") : 666;
                String errmsg = null != jsonObject.get("errmsg") ? (String) jsonObject.get("errmsg") : "";
                if (0 == errcode && "ok".equals(errmsg)) {
                    JSONObject objJson = null != jsonObject.get("result") ? (JSONObject) jsonObject.get("result") : null;
                    boolean has_more=null != objJson.get("has_more") ? (boolean) objJson.get("has_more") : false;
                    Integer next_cursor=null != objJson.get("next_cursor") ? (Integer) objJson.get("next_cursor") : 0;
                    if(has_more){
                        cursor=next_cursor;
                    }else{
                        mark=false;
                    }
                    JSONArray deptIds = null != objJson ? (JSONArray) objJson.get("list") : null;
                    if (null != deptIds && deptIds.size() > 0) {
                        for (int i = 0; i < deptIds.size(); i++) {
                            JSONObject d = (JSONObject) deptIds.get(i);
                            users.add(d);

                        }
                    }
                }else{
                    log.error("shibai");
                    mark=false;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return users;
    }

    public static String sendOaMsg(String accessToken,String agentId,  String useridList, String url, String title, String content, HashMap<String,String> form){


            try {
                DingTalkClient client = new DefaultDingTalkClient("https://oapi.dingtalk.com/topapi/message/corpconversation/asyncsend_v2");
                OapiMessageCorpconversationAsyncsendV2Request req = new OapiMessageCorpconversationAsyncsendV2Request();
                req.setAgentId(Long.valueOf(agentId));//1783738475L
                req.setUseridList(useridList);//"061861562132840542"
                OapiMessageCorpconversationAsyncsendV2Request.Msg obj1 = new OapiMessageCorpconversationAsyncsendV2Request.Msg();
                obj1.setMsgtype("oa");
//                OapiMessageCorpconversationAsyncsendV2Request.Text obj2 = new OapiMessageCorpconversationAsyncsendV2Request.Text();
//                obj2.setContent("oa消息通知2");
//                obj1.setText(obj2);
//                OapiMessageCorpconversationAsyncsendV2Request.Link obj3 = new OapiMessageCorpconversationAsyncsendV2Request.Link();
//                obj3.setTitle("我是标题113");
//                obj1.setLink(obj3);
                OapiMessageCorpconversationAsyncsendV2Request.OA obj4 = new OapiMessageCorpconversationAsyncsendV2Request.OA();
                OapiMessageCorpconversationAsyncsendV2Request.Body obj5 = new OapiMessageCorpconversationAsyncsendV2Request.Body();
//                obj5.setAuthor("工单宝");
                obj5.setContent(content);
                List<OapiMessageCorpconversationAsyncsendV2Request.Form> list7 = new ArrayList<OapiMessageCorpconversationAsyncsendV2Request.Form>();
                if(null!=form){
                    for(Map.Entry<String,String> entry : form.entrySet()){
                        OapiMessageCorpconversationAsyncsendV2Request.Form tmp = new OapiMessageCorpconversationAsyncsendV2Request.Form();
                        tmp.setValue(entry.getValue());
                        tmp.setKey(entry.getKey());
                        list7.add(tmp);
                    }
                }

                obj5.setForm(list7);
                obj5.setTitle(title);
                obj4.setBody(obj5);
                OapiMessageCorpconversationAsyncsendV2Request.Head obj9 = new OapiMessageCorpconversationAsyncsendV2Request.Head();
                obj9.setBgcolor("FFBBBBBB");
                obj9.setText("oa消息通知");
                obj4.setHead(obj9);
//                if(StringUtil.isNullOrEmpty(url)){
//                    url="http://www.baidu.com";
//                }
                obj4.setMessageUrl(url);
                obj1.setOa(obj4);
//                OapiMessageCorpconversationAsyncsendV2Request.Markdown obj10 = new OapiMessageCorpconversationAsyncsendV2Request.Markdown();
//                obj10.setTitle("我是标题1");
//                obj1.setMarkdown(obj10);
//                OapiMessageCorpconversationAsyncsendV2Request.ActionCard obj11 = new OapiMessageCorpconversationAsyncsendV2Request.ActionCard();
//                List<OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList> list13 = new ArrayList<OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList>();
//                OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList obj14 = new OapiMessageCorpconversationAsyncsendV2Request.BtnJsonList();
//                list13.add(obj14);
//                obj14.setTitle("我是标题2");
//                obj11.setBtnJsonList(list13);
//                obj11.setTitle("我是标题3");
//                obj1.setActionCard(obj11);
                req.setMsg(obj1);
//                if(StringUtil.isNullOrEmpty(accessToken)){
//                    accessToken=Sample.getAccessToken();
//                }
                OapiMessageCorpconversationAsyncsendV2Response rsp = client.execute(req, accessToken);
//                System.out.println(rsp.getBody());
                return(rsp.getBody());
            } catch (Exception e) {
                log.error(e.getMessage(),e);
//                e.printStackTrace();
                return e.getMessage();
            }
//        return null;
    }

}
